module.exports = {
    apps : [
        {
          name: "backend",
          script: "./app.js",
          watch: true,
          env: {
              "NODE_ENV": "production"
          },
        }
    ]
  }