const mongoose = require('mongoose')

const adminSchema = mongoose.Schema({
    userName: {
        type: 'String',
        required: true,
        unique: true
    },

    password: {
        type: String,
        required: true,
        default: '',
        select: false
    },

    employeeList: [
        {
            type: mongoose.Types.ObjectId,
            ref: 'Employee'
        }
    ],

    superAdmin: {
        type: Boolean,
        required: true
    },

    shopName: {
        type: String,
        required: true
    },

    street1: {
        type: 'String',
        required: true,
        default: ''
    },
    street2: {
        type: 'String',
        default: ''
    },
    city: {
        type: 'String',
        required: true,
        default: ''
    },
    state: {
        type: 'String',
        required: true,
        default: ''
    },
    postalCode: {
        type: 'String',
        required: true,
        default: ''
    },
    country: {
        type: 'String',
        required: true,
        default: ''
    },
    shopTel: {
        type: 'String',
        required: true,
        default: ''
    }
})

const AdminModel = mongoose.model('Admin', adminSchema)

module.exports = AdminModel
