const mongoose = require("mongoose")

const FindPasswordSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    tmpCode: {
        type: String,
        required: true
    }
}, {timestamps: true})

FindPasswordSchema.index({createdAt: 1},{expireAfterSeconds: 300})

const FindPasswordModel = mongoose.model('FindPassword', FindPasswordSchema)
module.exports = FindPasswordModel