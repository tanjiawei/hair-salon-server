const Mongoose = require("mongoose")

//Room's schema
const AppSettingSchema = Mongoose.Schema({
    type: {
        type: String,
        required: true,
        enum: ['ReloadMoney', 'Ads', 'Points'],
        default: ''
    },
    content: {},
})

const AppSettingModel = Mongoose.model('AppSetting', AppSettingSchema)
module.exports = AppSettingModel