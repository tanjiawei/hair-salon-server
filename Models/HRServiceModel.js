const mongoose = require("mongoose");

const serviceSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    default: "",
  },
  duration: {
    type: String,
    required: true,
    default: "",
  },
  price: {
    type: Number,
    required: true,
    default: "",
  },
  description: {
    type: String,
    default: "",
  },
  adminid: {
    type: mongoose.Schema.Types.ObjectId,
  },
  categoryId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "HRServiceCategory",
    required: true,
    default: "",
  },
  roomId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Room",
    //required: true,
    default: "",
  },
});
const HRServiceModel = mongoose.model("Service", serviceSchema);
module.exports = HRServiceModel;
