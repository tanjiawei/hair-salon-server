const Mongoose = require('mongoose')

//Event's schema
const EventSchema = Mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    userId: {
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },
    shopId: {
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'Admin',
        required: true
    },
    shopAddress: {
        type: String
    },
    message: {
        type: String
    },
    employeeId: {
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'Employee',
        required: true
    },
    selectedEmployee: {},
    serviceId: {
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'Service',
        required: true,
        default: ''
    },
    selectedService: {},
    day: {
        type: Date,
        required: true
    },
    start: {
        type: Date,
        required: true
    },
    end: {
        type: Date,
        required: true
    },
    duration: {
        type: String,
        required: true
    },
    createAt: {
        type: Date,
        default: Date.now
    },
    status: {
        type: String,
        default: '',
        enum: [
            '',
            'arrived',
            'started',
            'completed',
            'cancelled',
            null
        ]
    },
    statusMessage: {
        type: String,
        default: ''
    },
    orderId: {
        type: String,
        default: ''
    }
})

const EventModel = Mongoose.model('Event', EventSchema)
module.exports = EventModel
