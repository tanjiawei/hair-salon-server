const mongoose = require('mongoose')

const CentralMerchandiseCategorySchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        default:
            'Category' +
            new Date().toISOString().slice(20, 24)
    },
    imgLink: {
        type: String
    }
})

const MerchandiseCategoryModel = mongoose.model(
    'CentralMerchandiseCategory',
    CentralMerchandiseCategorySchema
)
exports.MerchandiseCategorySchema = CentralMerchandiseCategorySchema
module.exports = MerchandiseCategoryModel
