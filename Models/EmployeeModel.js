const mongoose = require("mongoose")

// user schema
const employeeSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        default: ""
    },

    email: {
        type: String,
        required: true,
        default: ""
    },

    passwordSalt: {
        type: String,
        required: true,
        default: "",
        select: false
    },

    password: {
        type: String,
        required: true,
        default: "",
        select: false
    },

    address: {
        type: String,
        required: false,
        default: ""
    },

    phone: {
        type: String,
        required: false,
        default: ""
    },

    adminid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Admin"
    },

    createTime: {
        type: Date,
        default: Date.now
    },

    services: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Service",
        required: true,
        default: []
    }],

    categoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Category",
        required: true,
        default: ""
    },

    schedule: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Schedule",
        required: false,
        default: []
    }],

    tokens: [],
})

const employeeModel = mongoose.model('Employee', employeeSchema)

module.exports = employeeModel

// const temp = {
//     schedule: {
//         week1: {
//             monday: {
//                 workStart: 9,
//                 workEnd: 5,
//                 bookedTime: [
//                     { start: 10, end: 11, appointment: "object id linking to appointment collection" },
//                     { start: 11.5, end: 12, appointment: "object id linking to appointment collection" },
//                     { start: 15, end: 16, appointment: "object id linking to appointment collection" },
//                 ]
//             },
//             tuesday: {
//                 workStart: -1,
//                 wordEnd: -1,
//                 bookedTime: []
//             },
//             wednesday: {
//                 workStart: 9,
//                 workEnd: 5,
//                 bookedTime: [
//                     { start: 10, end: 11, appointment: "object id linking to appointment collection" },
//                     { start: 11.5, end: 12, appointment: "object id linking to appointment collection" },
//                     { start: 15, end: 16, appointment: "object id linking to appointment collection" },
//                 ]
//             }
//         }
//     }
// }