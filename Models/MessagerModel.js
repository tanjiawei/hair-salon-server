const mongoose = require('mongoose');

const MessagerSchema = mongoose.Schema({
    shopId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Admin'
    },
    employeeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee'
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    type: {
        type: String,
        required: true,
        enum: ['user', 'employee']
    },
    message: {
        type: String,
        required: true,
        default: ''
    },
    flag: {
        type: String,
        required: true,
        default: 'unread',
        enum: ['read', 'unread']
    },
    createTime: {
        type: Date,
        default: Date.now
    },
    statusMessage: {
        type: String,
        default: ''
    },
    messageType: {
        type: String,
        default: ''
    },
    orderId: {
        type: String,
        default: ''
    }
});

const MessagerModel = mongoose.model('Messager', MessagerSchema);

module.exports = MessagerModel;
