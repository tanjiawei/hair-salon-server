const mongoose = require('mongoose')
const { startCase } = require('lodash/fp')

// user schema
const userSchema = mongoose.Schema(
    {
        email: {
            type: String,
            unique: true,
            lowercase: true,
            minlength: 5,
            maxlength: 255
        },

        passwordSalt: {
            type: String,
            required: true,
            default: '',
            select: false
        },

        password: {
            type: String,
            required: true,
            default: '',
            select: false
        },

        name: {
            type: String,
            trim: true,
            required: true,
            default: 'User' + Date.now(),
            get: startCase
        },

        phone: {
            type: String,
            trim: true
        },

        shippingAddress: {
            type: 'String'
        },

        balance: {
            type: 'Number',
            required: true,
            default: 0
        },

        points: {
            type: 'Number',
            required: true,
            default: 0
        },

        profileImage: {
            type: String,
            required: false,
            default: ''
        },

        points: {
            type: 'Number',
            required: true,
            default: 50
        },

        address: [],

        tokens: []
    },
    {
        toJSON: {
            getters: true
        }
    }
)

const userModel = mongoose.model('User', userSchema)

module.exports = userModel
