const mongoose = require('mongoose')
const EventSchema = require('./EventModel')

const OrderSchema = mongoose.Schema(
    {
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: false,
        },
        orderDate: {
            type: Date,
            default: Date.now
        },
        shopId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Admin'
        },
        // eventItems: [
        //   {
        //     type: mongoose.Schema.Types.ObjectId,
        //     ref: "Event",
        //     default: [],
        //   },
        // ],
        // merchandiseItems: [
        //   {
        //     type: mongoose.Schema.Types.ObjectId,
        //     ref: "Merchandise",
        //     default: [],
        //     unique: true,
        //   },
        // ],
        centralMerchandiseItems: {
            type: mongoose.Schema.Types.Mixed,
            default: {}
        },
        orderMerchandiseItems: {
            type: mongoose.Schema.Types.Mixed,
            default: {}
        },
        orderEventItems: {
            type: mongoose.Schema.Types.Mixed,
            default: {}
        },
        orderSubtotal: {
            type: Number,
            default: 0,
            min: 0
        },
        orderTax: {
            type: Number,
            default: 0,
            min: 0
        },
        orderDelivery: {
            type: Number,
            default: 0,
            min: 0
        },
        tipsAmount: {
            type: Number,
            default: 0,
            min: 0
        },
        orderAmount: {
            type: Number,
            default: 0,
            min: 0
        },
        orderSubtotalPoint: {
            type: Number,
            default: 0,
            min: 0
        },
        orderId: {
            type: String,
            unique: true
        },
        status: {
            type: String,
            default: 'PENDING',
            enum: [
                'WAITINGPAY',
                'PENDING',
                'PAID',
                'SHIPPED',
                'RETURNED',
                'CANCELED'
            ]
        },
        type: {
            type: String,
            enum: [
                'ONLINE',
                'OFFLINE',
            ]
        },
        shippingAddress: {},
        deliveryOption: {},
        isPickup: {
            type: Boolean,
            default: false
        },
        shipment: {},
        createdAt: {
            type: Date,
            default: Date.now
        },
        employeeId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Employee'
        }

        // paymentInfo: {
        //   type: mongoose.Schema.Types.ObjectId,
        //   ref: "Payment",
        // },
        // discount: {
        //   type: Number,
        //   default: 0,
        // },
        // federalTax: {
        //   type: Number,
        //   default: 0,
        //   min: 0,
        // },
        // provincialTax: {
        //   type: Number,
        //   default: 0,
        //   min: 0,
        // },
    },
    { minimize: false }
)

// OrderSchema.virtual("events", {
//   ref: "Event",
//   localField: "_id",
//   foreignField: "order",
//   justOne: false,
// });

// OrderSchema.pre("save", async function (next) {
//   this.populate();

//   next();
// });

module.exports = mongoose.model('Order', OrderSchema)
