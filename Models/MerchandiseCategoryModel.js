const mongoose = require('mongoose')

//Merchandise category's schema
const MerchandiseCategorySchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        default: 'Category' + new Date().toISOString().slice(20, 24)
    },
    adminid: {
        type: mongoose.Types.ObjectId
    },
    imgLink: {
        type: String
    }
})

const MerchandiseCategoryModel = mongoose.model('MerchandiseCategory', MerchandiseCategorySchema)
exports.MerchandiseCategorySchema = MerchandiseCategorySchema
module.exports = MerchandiseCategoryModel
