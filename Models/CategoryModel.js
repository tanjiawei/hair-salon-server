const mongoose = require("mongoose")

// Employee category schema
const categorySchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        default: ""
    },
    adminid: {
        type: mongoose.Types.ObjectId,
    },
})

const categoryModel = mongoose.model('Category', categorySchema)

module.exports = categoryModel
