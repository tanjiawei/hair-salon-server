const mongoose = require('mongoose')

const ReservationSchema = mongoose.Schema({
    shopId: {
        type: mongoose.Schema.Types.ObjectId,
        required: false,
        ref: "Admin"
    },

    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    
    userInfo: {

    },

    orderId: {
        type: String,
        required: true,
        default: ""
    },


    type: {
        type: String,
        required: true,
        default: ""
    },

    events: [],
    createTime: {
        type: Date,
        default: Date.now
    },
})

const ReservationModel = mongoose.model("Reservation", ReservationSchema)
module.exports = ReservationModel