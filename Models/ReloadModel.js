const mongoose = require("mongoose")

const reloadSchema = mongoose.Schema({
    value: {
        type: String,
        required: true,
        default: ""
    },
    bonus: {
        type: String,
        default: ""
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    status: {
        type: String,
        enum: ['waitting', "failed", 'paid',],
        default: "waitting"
    },
    paymentDetail: {

    },
    createTime: {
        type: Date,
        default: Date.now
    },
})

const reloadModel = mongoose.model('Reload', reloadSchema)

module.exports = reloadModel