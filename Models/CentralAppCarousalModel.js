const mongoose = require('mongoose')

const CentralAppCarousalSchema = new mongoose.Schema({
    merchandise: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'CentralMerchandise',
        required: true,
        unique: true
    },
    imageIndex: {
        type: Number,
        required: true,
        min: 1,
        default: 1
    }
})

const CentralAppCarousalModel = mongoose.model(
    'CentralAppCarousal',
    CentralAppCarousalSchema
)
module.exports = CentralAppCarousalModel
