const Mongoose = require("mongoose")

//HRService category's schema
const HRServiceCategorySchema = Mongoose.Schema({
    name: {
        type: String,
        required: true,
        default: ''
    },
    adminid: {
        type: Mongoose.Schema.Types.ObjectId,
    },
})

const HRServiceCategoryModel = Mongoose.model('HRServiceCategory', HRServiceCategorySchema)
module.exports = HRServiceCategoryModel