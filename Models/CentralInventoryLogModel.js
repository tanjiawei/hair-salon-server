const mongoose = require('mongoose')

const CentralInventoryLogSchema = new mongoose.Schema({
    merchandise: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'CentralMerchandise',
        required: true
    },
    dateIn: {
        type: Date,
        required: true,
        default: Date.now
    },
    stockLog: {
        type: Number,
        required: true
    },
    totalStock: {
        type: Number,
        required: false
    },
    type: {
        type: String,
        default: "add",
        enum: [
            'order',
            'add',
        ]
    },
    orderId: {
        type: String,
        default: ''
    },
    note: {
        type: String,
        default: ''
    },
})

const CentralInventoryLogModel = mongoose.model(
    'CentralInventoryLog',
    CentralInventoryLogSchema
)
module.exports = CentralInventoryLogModel
