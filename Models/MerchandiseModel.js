const mongoose = require("mongoose");

const MerchandiseSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  categoryId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "MerchandiseCategory",
    required: false,
    default: "",
  },
  // orderQuantity: {
  //   type: Number,
  //   default: 0,
  //   min: 0,
  // },
  price: {
    type: Number,
    required: true,
  },
  stock: {
    type: Number,
    required: true,
  },
  minStock: {
    type: Number,
    required: false,
    default: 0,
  },
  note: {
    type: String,
    required: false,
  },
  imgURL: {
    type: [String],
    required: true,
    default: [],
  },
  isValid: {
    type: Boolean,
    required: true,
    default: true,
  },
  isForService: {
    type: Boolean,
    required: true,
    default: false
  },
  adminid: {
    type: mongoose.Types.ObjectId,
  },
});

const MerchandiseModel = mongoose.model("Merchandise", MerchandiseSchema);
module.exports = MerchandiseModel;
