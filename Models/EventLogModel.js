const Mongoose = require("mongoose");

const EventLogSchema = Mongoose.Schema({
  orderId: {
    type: String,
    required: true,
  },
  eventId: {
    type: Mongoose.Schema.Types.ObjectId,
    ref: "Event",
    required: true,
  },
  shopId: {
    type: Mongoose.Schema.Types.ObjectId,
    ref: "Admin",
    required: true,
  },
  status: {
    type: String,
    default: "",
    enum: ["", "arrived", "started", "completed", "cancelled", null],
  },
  statusMessage: {
    type: String,
    default: ""
  },
  createAt: {
    type: Date,
    default: Date.now,
  },
});

const EventLogModel = Mongoose.model("EventLog", EventLogSchema);
module.exports = EventLogModel;
