const mongoose = require('mongoose');

const serviceBondSchema = mongoose.Schema({
    employeeId: {
        type: mongoose.Types.ObjectId,
        required: true,
        default: ""
    },
    serviceId: {
        type: mongoose.Types.ObjectId,
        required: true,
        default: ""
    },
    price: {
        type: Number,
        required: true,
        default: "0"
    }
});
const HRServiceBondModel = mongoose.model('ServiceBond',serviceBondSchema)
module.exports = HRServiceBondModel
