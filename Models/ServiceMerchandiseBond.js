const mongoose = require('mongoose')

const ServiceMerchandiseBondSchema = new mongoose.Schema({
    serviceId: {
        type: mongoose.ObjectId,
        ref: 'Service',
        required: true
    },
    merchandiseId: {
        type: mongoose.ObjectId,
        ref: 'Merchandise',
        required: true
    },
    usage: {
        type: Number,
        required: true
    }
})

const ServiceMerchandiseBondModel = mongoose.model('SMBound', ServiceMerchandiseBondSchema)
module.exports = ServiceMerchandiseBondModel
