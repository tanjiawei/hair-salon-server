const mongoose = require("mongoose")

const InventoryLogSchema = new mongoose.Schema({
    merchandiseId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Merchandise",
        required: true
    },
    employeeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Employee",
    },
    adminId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Admin",
    },
    type: {
        type: String,
        default: "add",
        enum: [
            'service',
            'order',
            'add',
        ]
    },
    orderId: {
        type: String,
    },
    stockLog: {
        type: Number,
        required: true,
    }
}, {
    strict: false,
    timestamps: true,
})

const InventoryLogModel = mongoose.model('InventoryLog', InventoryLogSchema)
module.exports = InventoryLogModel