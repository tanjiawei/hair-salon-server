const Mongoose = require("mongoose")

//Room's schema
const RoomSchema = Mongoose.Schema({
    name: {
        type: String,
        required: true,
        default: ''
    },
    adminid: {
        type: Mongoose.Types.ObjectId,
    },
})

const RoomModel = Mongoose.model('Room', RoomSchema)
module.exports = RoomModel