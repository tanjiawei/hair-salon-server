const mongoose = require("mongoose")

// schedule schema
const scheduleSchema = mongoose.Schema({
    startTime: {
        type: Date,
        default: ""
    },

    endTime: {
        type: Date,
        default: ""
    },

    launchStart: {
        type: Date,
        default: ""
    },

    launchEnd: {
        type: Date,
        default: ""
    },

    day: {
        type: Date,
        default: ""
    },

    scheduleType: {
        type: String,
        default: ""
    },

    adminid: {
        type: mongoose.Schema.Types.ObjectId
    },
    
    employeeid: {
        type: mongoose.Schema.Types.ObjectId
    },
})

const scheduleModel = mongoose.model('Schedule', scheduleSchema)

module.exports = scheduleModel