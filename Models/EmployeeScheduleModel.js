const mongoose = require("mongoose")

// employeeSchedule schema
const employeeScheduleSchema = mongoose.Schema({
    name: {
        type: String,
        default: ""
    },
    schedules: {
        startTime: {
            type: String,
            default: ""
        },
        // endTime: {
        //     type: String,
        //     default: ""
        // },
        // launchStart:{
        //     type: String,
        //     default: ""
        // },
        // launchEnd:{
        //     type: String,
        //     default: ""
        // },
        // day: {  
        //     type: String,
        //     default: ""
        // },
        // scheduleType:{
        //     type: String,
        //     default: ""
        // },
        // employeeid: {
        //     type: mongoose.Schema.Types.ObjectId
        // },
    }
   
})

const employeeScheduleModel = mongoose.model('employees', employeeScheduleSchema)

module.exports = employeeScheduleModel