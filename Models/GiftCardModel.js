const mongoose = require("mongoose")

const giftCardSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        default: "Gift Card",
    },
    value: {
        type: Number,
        required: true,
        default: 0,
    },
    price: {
        type: Number,
        required: true,
        default: 0,
    },
    amount: {
        // Total amount of the gift cards of this type
        type: Number,
        required: true,
        default: 0,
    },
    redeemed: {
        // The amount of the gift cards already used.
        type: Number,
        required: true,
        default: 0,
    },
    note: {
        type: String,
        require: true,
        default: "",
    },
    imgURL: {
        type: String,
    },
    isValid: {
        type: Boolean,
        required: true,
        default: true,
    }
})

const giftCardModel = mongoose.model('GiftCard', giftCardSchema)
module.exports = giftCardModel
