const mongoose = require('mongoose')

const CentralMerchandiseSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        minLength: 1,
        maxlength: 255,
        unique: true
    },
    sku: {
        type: String,
        required: false,
        minLength: 8,
        unique: true
    },
    categoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'CentralMerchandiseCategory',
        required: false,
        default: ''
    },
    price: {
        type: Number,
        required: true,
        min: 0,
        default:0
    },
    note: {
        type: String,
        default: '',
        maxlength: 255
    },
    priceWithPoint: {
        type: Number,
        default: function (n) {
            return n ?? this.price
        }
    },
    point: {
        type: Number,
    },
    stock: {
        type: Number,
        required: true,
        min: 0
    },
    weight: {
        type: Number,
        required: false,
        min: 0,
        default: 0,
    },
    minStock: {
        type: Number,
        required: false,
        default: 0,
        min: 0
    },
    numOfOrders: {
        type: Number,
        default: 0,
        min: 0
    },
    descriptions: {
        type: [
            new mongoose.Schema({
                subTitle: {
                    type: String,
                    minlength: 1,
                    maxlength: 100,
                    required: true,
                    trim: true
                },
                note: {
                    type: [String],
                    minlength: 1,
                    maxlength: 255,
                    default: '',
                    trim: true
                }
            })
        ],
        default: []
    },
    imgURL: {
        type: [String],
        required: true,
        default: []
    },
    isValid: {
        type: Boolean,
        required: true,
        default: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})

const CentralMerchandiseModel = mongoose.model('CentralMerchandise', CentralMerchandiseSchema)
module.exports = CentralMerchandiseModel
