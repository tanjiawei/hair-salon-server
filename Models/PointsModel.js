const mongoose = require("mongoose")

const reloadSchema = mongoose.Schema({
    value: {
        type: String,
        required: true,
        default: ""
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    type:{
        type: String,
        enum: ['share', "service", 'complete',],
        default: ""
    }
}, {
    strict: false,
    timestamps: true,
})

const pointsModel = mongoose.model('Point', reloadSchema)

module.exports = pointsModel