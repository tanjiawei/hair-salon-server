const mongoose = require("mongoose")

// user schema
const shopOperationSchema = mongoose.Schema({
    adminId: {
        type: mongoose.Types.ObjectId,
        required: true,
    },

    shopDate: {
        type: String,
        required: true,
        default: "",
    },

    shopOpenTime: {
        type: String,
        required: true,
        default: "",
    },

    shopCloseTime: {
        type: String,
        required: true,
        default: "",
    },
    
    shopOpen: {
        type: String,
        required: true,
        default: "",
    },
   
    shopCloseDesc: {
        type: String,
        required: true,
        default: "",
    },
})

const ShopOperationModel = mongoose.model('ShopOperation', shopOperationSchema)

module.exports = ShopOperationModel