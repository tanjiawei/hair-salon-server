const mongoose = require('mongoose')
const mongooseLeanVirtuals = require('mongoose-lean-virtuals')
const { capitalize } = require('lodash')
const CONFIG = require('../CONFIG')

const FeedingsSchema = new mongoose.Schema(
    {
        user: {
            type: new mongoose.Schema({
                name: {
                    type: String,
                    required: true,
                    minlength: 1,
                    maxlength: 255
                },
                points: {
                    type: Number,
                    default: 0
                },
                profileImage: {
                    type: String
                }
            }),
            required: true
        },
        createdAt: {
            type: Date,
            default: Date.now
        },
        imgURI: {
            type: [String],
            required: [true, 'Please choose at least one image.'],
            validate: {
                validator: function (a) {
                    return a.length > 0
                },
                message: 'At lease one image is required.'
            },
            default: [],
            maxlength: CONFIG.feedingImageLimit
        },
        description: {
            type: String,
            default: '',
            maxlength: 255,
            trim: true,
            get: (v) => capitalize(v)
        },
        liked: {
            type: [
                {
                    type: new mongoose.Schema({
                        name: {
                            type: String,
                            required: true,
                            minlength: 1,
                            maxlength: 255,
                            default: 'Anonymous user'
                        }
                    })
                }
            ],
            default: []
        }
    },
    {
        toJSON: {
            getters: true
        }
    }
)

FeedingsSchema.virtual('allLikes').get(function () {
    return this.liked.length
})

FeedingsSchema.plugin(mongooseLeanVirtuals)

const FeedingsModel = mongoose.model('Feeding', FeedingsSchema)

module.exports = FeedingsModel
