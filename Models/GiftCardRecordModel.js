const mongoose = require("mongoose")
const giftCardModel = require("./GiftCardModel")

const GiftCardRecordSchema = mongoose.Schema({
    giftCardId: {
        type: mongoose.ObjectId,
        ref: "GiftCard",
        required: true,
    },
    senderId: {
        type: mongoose.ObjectId,
        ref: "User",
        required: true,
    },
    senderName: String,
    receiverName: String,
    receiverEmail: String,
    message: String,
})

const GiftCardRecordModel = mongoose.model("GiftCardRecord", GiftCardRecordSchema)
module.exports = GiftCardRecordModel
