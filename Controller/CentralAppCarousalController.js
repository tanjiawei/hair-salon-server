const mongoose = require('mongoose')
const Model = require('../Models/CentralAppCarousalModel')
const CentralMerchandiseModel = require('../Models/CentralMerchandiseModel')
const httpMsg = require('../Util/httpCustomMessage')

const CentralAppCarousalController = {
    getListings: async (req, res) => {
        const recordList = await Model.find().populate(
            'merchandise',
            ['imgURL', 'isValid', 'name', 'stock']
        )

        res.send(recordList)
    },

    getAvailableListings: async (req, res) => {
        const list = await CentralMerchandiseModel.find({
            _id: {
                $nin: req.body.currentList
            },
            isValid: true
        })
            .populate('categoryId', 'name')
            .select('name stock numOfOrders imgURL isValid')

        res.send(list)
    },

    postListings: async (req, res) => {
        const { merchandise, imageIndex } = req.body

        await validateRequestRecord(req, res)

        const record = new Model({
            merchandise,
            imageIndex
        })
        await record.save()

        res.send(record)
    },

    editListings: async (req, res) => {
        const record = await Model.findByIdAndUpdate(
            req.params.id,
            {
                imageIndex: req.body.imageIndex
            },
            { new: true }
        )

        res.send(record)
    },

    deleteListings: async (req, res) => {
        const record = await Model.findByIdAndRemove(
            req.params.id
        )

        if (!record)
            return res.status(404).send(httpMsg.M404)

        res.send(httpMsg.M200)
    }
}

module.exports = CentralAppCarousalController

async function validateRequestRecord(req, res) {
    const { merchandise } = req.body
    if (!mongoose.Types.ObjectId.isValid(merchandise))
        return res.status(400).send(httpMsg.M400)

    const result = await CentralMerchandiseModel.findById(
        merchandise
    )
    if (!result) return res.status(404).send(httpMsg.M404)
}
