const EventModel = require('../Models/EventModel.js');
const Mongoose = require('mongoose');

const EventController = {
    // Fetch all the exist Event
    getAllEvents: async (req, res) => {
        let EventList = await EventModel.find({});
        res.json(EventList);
        return;
    },

    // Add new Event by name.
    addEvent: async (req, res) => {
        let newEntry = new EventModel({
            employeeId: req.body.employeeId,
            serviceId: req.body.serviceId,
            day: req.body.day,
            start: req.body.start,
            duration: req.body.duration
        });
        console.log(req.body);
        newEntry
            .save()
            .then(() => {
                res.status(200).send('Save Success');
            })
            .catch((err) => {
                console.error('Fail to save');
                res.status(500).send('Internal Server Error (' + err + ')');
            });
    },

    // Update an exist Event.
    updateEvent: async (req, res) => {
        const result = await EventModel.updateOne(
            { _id: req.body._id },
            {
                $set: {
                    employeeId: req.body.employeeId,
                    serviceId: req.body.serviceId,
                    duration: req.body.duration
                }
            }
        );
        res.status(200).send(result);
    },

    // Delete Event by _id
    deleteEvent: async (req, res) => {
        await EventModel.deleteOne({
            _id: Mongoose.Types.ObjectId(req.params._id)
        });
        res.status(200).send('Delete Success');
    },

    getEmployeeDateEvent: async (req, res) => {
        const EventList = await EventModel.find({
            shopId: Mongoose.Types.ObjectId(req._id),
            employeeId: Mongoose.Types.ObjectId(req.body.employeeId),
            day: {
                $gte: new Date(req.body.dayFrom),
                $lt: new Date(req.body.dayTo)
            }
        });
        res.json(EventList);
    }
};

module.exports = EventController;
