const mongoose = require('mongoose')
const fs = require('fs')
const _ = require('lodash')
const { v4: uuidv4 } = require('uuid')

const CentralMerchandiseModel = require('../Models/CentralMerchandiseModel')
const httpMsg = require('../Util/httpCustomMessage')

const createSku = (input, length = 12) =>
    _(input)
        .chain()
        .trim()
        .words()
        .take(4)
        .map((str) => str.slice(0, 2))
        .concat(_.trim(uuidv4(), '-'))
        .join('')
        .toUpper()
        .value()
        .slice(0, length)

const CentralMerchandiseController = {
    bestSellers: async (req, res) => {
        const { criteria = 0, limit = 0 } = req.body
        const merchandiseList = await CentralMerchandiseModel.find({
            isValid: true,
            price: { $gte: criteria }
        })
            .sort('-numOfOrders')
            .limit(limit)

        res.send(merchandiseList)
    },

    getAllMerchandises: async (req, res) => {
        const merchandiseList = await CentralMerchandiseModel.find({
            isValid: true
        })
            .sort('-createdAt')
            .lean()

        res.send(merchandiseList)
    },

    getMerchandisebyId: async (req, res) => {
        const merchandiseList = await CentralMerchandiseModel.find({
            isValid: true,
            _id: req.params._id
        })

        res.send(merchandiseList[0])
    },

    getMerchandise: async (req, res) => {
        const merchandiseList = await CentralMerchandiseModel.aggregate([
            {
                $match: {
                    isValid: true
                }
            },
            {
                $lookup: {
                    from: 'centralmerchandisecategories',
                    localField: '_id',
                    foreignField: 'categoryId',
                    as: 'docCategory'
                }
            }
        ])

        res.send(merchandiseList)
    },

    getMerchandiseByCategoryId: async (req, res) => {
        const merchandiseList = await CentralMerchandiseModel.aggregate([
            {
                $match: {
                    isValid: true,
                    categoryId: mongoose.Types.ObjectId(req.body.categoryId)
                }
            },
            {
                $lookup: {
                    from: 'centralmerchandisecategories',
                    localField: '_id',
                    foreignField: 'categoryId',
                    as: 'docCategory'
                }
            }
        ])

        res.send(merchandiseList)
    },

    getShortageList: async (req, res) => {
        const merchandiseList = await CentralMerchandiseModel.find({
            isValid: true,
            $where: 'this.minStock > this.stock'
        })

        res.send(merchandiseList)
    },

    addMerchandise: async (req, res) => {
        req.body.imgURL = req.body.imgURL ? JSON.parse(req.body.imgURL) : []

        const {
            name,
            sku,
            categoryId,
            price,
            priceWithPoint,
            point,
            stock,
            minStock,
            weight,
            note,
            imgURL
        } = req.body

        if (req.files && req.files.length > 0) {
            req.files.map((each) => {
                imgURL.push(each.destination + each.filename)
            })
        }

        const merchandise = new CentralMerchandiseModel({
            name,
            categoryId,
            price,
            stock,
            minStock,
            weight,
            imgURL,
            sku: sku || createSku(name),
            priceWithPoint: priceWithPoint || price,
            point: point || 0,
            note: note || ''
        })
        await merchandise.save()

        res.send(merchandise)
    },

    updateMerchandise: async (req, res) => {
        const doc = await CentralMerchandiseModel.findById(req.params._id)
        if (!doc) return res.status(404).send(httpMsg.M404)

        req.body.imgURL = req.body.imgURL ? JSON.parse(req.body.imgURL) : []

        if (_.difference(req.body.imgURL, doc.imgURL).length > 0)
            return res.status(400).send(httpMsg.M400)

        const deleted = _.differenceWith(doc.imgURL, req.body.imgURL, _.isEqual)

        if (req.files.length > 0) {
            req.files.map((each) => {
                req.body.imgURL.push(each.destination + each.filename)
            })
        }
        const { sku, priceWithPoint, point, note, price, name } = req.body
        const Merchandise = await CentralMerchandiseModel.findByIdAndUpdate(
            req.params._id,
            {
                $set: {
                    ...req.body,
                    sku: sku || createSku(name),
                    priceWithPoint: priceWithPoint || price,
                    point: point || 0,
                    note: note || ''
                }
            },
            { new: true }
        )
        if (deleted.length > 0) deleted.map((url) => fs.unlink(url, (err) => console.error(err)))

        res.send(Merchandise)
    },

    deleteMerchandise: async (req, res) => {
        const merchandise = await CentralMerchandiseModel.findByIdAndUpdate(req.params._id, {
            $set: { isValid: false, imgURL: [] }
        })

        if (!merchandise) return res.status(404).send(httpMsg.M404)

        if (merchandise.imgURL[0]) {
            merchandise.imgURL.map((url) => fs.unlink(url, (err) => console.error(err)))
        }

        res.send(httpMsg.M200)
    }
}

module.exports = CentralMerchandiseController
