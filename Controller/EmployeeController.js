const EmployeeModel = require('../Models/EmployeeModel');
const EventModel = require('../Models/EventModel');
const MessagerModel = require('../Models/MessagerModel');
const OrderModel = require('../Models/OrderModel');
const UserModel = require('../Models/UserModel');

const mongoose = require('mongoose');

const MessagerService = require('../Service/MessagerService');

const JWT = require('jsonwebtoken');

const Auth = require('../Service/AuthService');

const employeeController = {
    login: async (req, res) => {
        const doc = await EmployeeModel.find({ email: req.body.email })
            .select('+password +passwordSalt')
            .exec();
        if (doc.length === 0) {
            res.status(200).json({ ok: false, error: 'Not a registered user' });
            return;
        }

        const user = doc[0];
        if (await Auth.checkPassword(req.body.password, user.password)) {
            const data = user.toObject();
            const jwt = JWT.sign(data._id.toString(), process.env.USER_JWT_PRIVATE_KEY);

            res.status(200).json({ data, jwt });
        } else {
            res.status(200).json({ error: 'Password missmatch' });
        }
    },

    loginWithJWT: async (req, res) => {
        const userData = await UserModel.EmployeeModel(req._id);
        res.json({ data: userData });
    },

    register: async (req, res) => {
        const doc = await EmployeeModel.find({ email: req.body.email });
        if (doc[0]) return res.status(400).send('Email already in use');

        const data = new EmployeeModel({
            email: req.body.email,
            passwordSalt: salt,
            password: await Auth.encryptPassword(req.body.password),
            adminid: '',
            categoryId: req.body.categoryId
        });

        const result = await data.save();
        res.status(200).send(result);
    },
    updateEmployee: async (req, res) => {
        const valuesToSet = {};
        Object.keys(req.body).forEach((key) => {
            valuesToSet[key] = req.body[key];
        });

        const employeeDocument = await EmployeeModel.updateOne(
            { _id: req.body._id },
            { $set: valuesToSet }
        );
        res.status(200).json(employeeDocument);
    },
    updateEmployeePassword: async (req, res) => {
        //verify if employee email exists
        const employeeResult = await EmployeeModel.find({
            email: req.body.email
        }).select('+password');

        if (employeeResult.length === 0)
            return res.status(400).send(`${req.body.email} is not registered`);

        let user = employeeResult[0];
        const isNewPasswordSame = await Auth.checkPassword(req.body.newPassword, user.password);
        const isCurrentPasswordValid = await Auth.checkPassword(req.body.password, user.password);

        if (isNewPasswordSame && isCurrentPasswordValid) {
            res.status(200).json({ error: 'New password is the same as current password' });
        } else if (isCurrentPasswordValid) {
            user.password = await Auth.encryptPassword(req.body.newPassword);

            await user.save({ validateBeforeSave: false });

            const jwt = JWT.sign(user._id.toString(), process.env.USER_JWT_PRIVATE_KEY);
            res.status(200).json({ ok: true, jwt });
        } else {
            res.status(200).json({ error: 'Password missmatch' });
        }
    },

    startEvent: async (req, res) => {
        const { employeeId, eventId } = req.body;
        console.log(employeeId);
        if (!employeeId || !eventId || employeeId.length < 1 || eventId.length < 1)
            return res.status(201).json({
                ok: false,
                error: 'invalid input'
            });

        let event = await EventModel.findById({ _id: eventId });

        if (event) {
            if (employeeId != event.employeeId) {
                return res.status(201).json({
                    ok: false,
                    error: 'No authorized'
                });
            }
            event.status = 'started';
            await event.save({ validateBeforeSave: false });
            return res.status(200).json({
                ok: true,
                data: event
            });
        } else {
            return res.status(201).json({
                ok: false,
                error: 'Event not found'
            });
        }
    },

    completeEvent: async (req, res) => {
        const { employeeId, eventId } = req.body;
        if (!employeeId || !eventId || employeeId.length < 1 || eventId.length < 1) {
            return res.status(201).json({
                ok: false,
                error: 'invalid input'
            });
        }

        let event = await EventModel.findById({ _id: eventId });
        event.status = 'completed';
        let orderDetail = await OrderModel.findOne({ orderId: event.orderId });
        if (orderDetail) {
            orderDetail.orderEventItems[eventId].status = 'completed';
            let isOrderComplete = true;
            Object.keys(orderDetail.orderEventItems).forEach((key) => {
                if (orderDetail.orderEventItems[key].status != 'completed') {
                    isOrderComplete = false;
                    return;
                }
            });
            if (isOrderComplete) {
                orderDetail.status = 'WAITINGPAY';

                if (orderDetail.user) {
                    MessagerService.sendMessageToUser(
                        {
                            shopId: orderDetail.shopId,
                            message: 'You have an order waitting to pay!',
                            messageType: 'order',
                            orderId: orderDetail.orderId
                        },
                        orderDetail.user,
                        'Your order is complete!',
                        'You can pay your order now!',
                        {
                            type: 'OrderDetail',
                            orderId: orderDetail.orderId
                        }
                    );
                }
            }
            await event.save({ validateBeforeSave: false });
            await orderDetail.save({ validateBeforeSave: false });
        } else {
            return res.status(201).json({
                ok: false,
                error: 'Order not found'
            });
        }
        return res.status(200).json({
            ok: true,
            data: event
        });
    },
    getRangeEmployeeEvents: async (req, res) => {
        if (req.body.startDate && req.body.endDate && req.body.employeeId) {
            const { startDate, endDate, employeeId } = req.body;

            const employeeAppointment = await EventModel.find({
                employeeId: employeeId,
                day: {
                    $gte: new Date(startDate),
                    $lte: new Date(endDate)
                }
            });
            return res.status(200).json({ ok: true, data: employeeAppointment });
        } else {
            return res.status(201).json({
                ok: false,
                error: 'invalid input'
            });
        }
    },

    bindFirebaseToken: async (req, res) => {
        let result = await EmployeeModel.updateOne(
            { _id: req.body.id },
            { $addToSet: { tokens: req.body.token } }
        );
        let docs = await EmployeeModel.findById(req.body.id);
        if (docs.tokens.length > 10) {
            docs.tokens.shift();
            await EmployeeModel.updateOne({ _id: req.body.id }, { $set: { tokens: docs.tokens } });
        }
        if (result && result.ok) {
            res.status(200).json({ message: 'success!' });
        } else {
            res.status(400).json({ error: 'failed!' });
        }
    },
    getMessages: async (req, res) => {
        let result = await MessagerModel.aggregate([
            {
                $match: { type: 'employee', employeeId: mongoose.Types.ObjectId(req.query.id) }
            }
        ])
            .sort({ createTime: -1 })
            .limit(20);

        if (result) {
            res.status(200).json(result);
        } else {
            res.status(400).json({ error: 'Internal Serve Error!' });
        }
    }
};

module.exports = employeeController;
