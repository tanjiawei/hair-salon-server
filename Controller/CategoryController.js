const mongoose = require('mongoose');
const categoryModel = require('../Models/CategoryModel');

const categoryController = {
    addCategory: async (req, res) => {
        const data = new categoryModel({
            name: req.body.name,
            adminid: req.body.adminId
        });

        const category = await data.save();
        res.status(200).send(category);
    },

    updateCategory: async (req, res) => {
        const categoryDocument = await categoryModel.updateOne(
            { _id: req.body._id },
            {
                $set: {
                    name: req.body.name
                }
            }
        );
        res.status(200).send(categoryDocument);
    },

    deleteCategory: async (req, res) => {
        await categoryModel.deleteOne({
            _id: mongoose.Types.ObjectId(req.body._id)
        });
        res.status(200).send('Delete Success');
    },

    getAllCategory: async (req, res) => {
        const categoryList = await categoryModel.find({
            adminid: mongoose.Types.ObjectId(req.query.id)
        });
        res.json(categoryList);
    }
};

module.exports = categoryController;
