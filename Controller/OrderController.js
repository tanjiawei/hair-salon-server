const OrderModel = require('../Models/OrderModel');
const EventModel = require('../Models/EventModel');
const UserModel = require('../Models/UserModel');
const MerchandiseModel = require('../Models/MerchandiseModel');
const mongoose = require('mongoose');
const { eventNames } = require('../Models/EventModel');
const orderid = require('order-id')('mysecret');
const UserService = require('../Service/UserService');
const { MerchandiseService } = require('../Service/MerchandiseService');

const OrderController = {
    getAllOrders: async (req, res) => {
        const orders = await OrderModel.find({}).populate('user');
        res.status(200).json({ ok: true, data: orders });
    },

    getOrdersByEmployeeId: async (req, res) => {
        const orders = await OrderModel.find({
            employeeId: req.params.id
        }).populate('user');
        console.log(req.params.id);
        if (!orders)
            return res.status(201).json({
                ok: false,
                message: 'error finding user orders'
            });

        res.status(201).json({
            ok: true,
            data: orders,
            count: orders.length
        });
    },
    getOrdersByUserId: async (req, res) => {
        const orders = await OrderModel.find({
            user: req.params.id
        })
            .populate('user')
            .sort({ orderDate: -1 });
        if (!orders) {
            return res.status(201).json({
                ok: false,
                message: 'error finding user orders'
            });
        }
        res.status(201).json({
            ok: true,
            data: orders,
            count: orders.length
        });
    },
    getTransaction: async (req, res, fu, skip = 0, limit = 10) => {
        const transactions = await OrderModel.aggregate([
            { $match: { user: mongoose.Types.ObjectId(req.params.id) } },
            {
                $project: {
                    _id: 0,
                    createdAt: 1,
                    amount: '$orderAmount',
                    type: 'order',
                    orderEventItems: 1
                }
            },
            {
                $unionWith: {
                    coll: 'reloads',
                    pipeline: [
                        { $match: { userId: mongoose.Types.ObjectId(req.params.id) } },
                        {
                            $project: {
                                createdAt: '$createTime',
                                _id: 0,
                                amount: '$value',
                                type: 'reload'
                            }
                        }
                    ]
                }
            },
            { $sort: { createdAt: -1 } },
            { $skip: parseInt(req.query.length) },
            { $limit: 10 }
        ]);
        if (!transactions) {
            return res.status(201).json({
                ok: false,
                message: 'error finding user transaction!'
            });
        }
        res.status(201).json({
            ok: true,
            data: transactions,
            count: transactions.length
        });
    },
    getSingleOrder: async (req, res) => {
        const orderId = req.params.id;

        const order = await OrderModel.findOne({
            orderId
        })
            .populate('eventItems')
            .populate('user');
        if (!order) {
            return res.status(201).json({
                ok: false,
                message: 'order not found'
            });
        }
        // await calculateOrderSubtotal(order)
        res.status(200).json({
            ok: true,
            data: order
        });
    },
    createOrder: async (req, res) => {
        const { user, eventItems, employeeId, merchandiseItems } = req.body;
        //need to check if userId exist and eventItems

        let shopId;
        if (eventItems && eventItems.length > 0) {
            let event = await EventModel.findById(eventItems);
            if (!event) {
                return res.status(201).json({
                    ok: false,
                    message: 'event not found or update to fail'
                });
            }
            if (event.orderId && event.orderId.length > 0) {
                return res.status(201).json({
                    ok: false,
                    message: 'event already belongs to the order'
                });
            }
            shopId = event.shopId;
        }
        let params = {};
        if (user) {
            params = {
                user,
                type: 'OFFLINE',
                orderId: orderid.generate(),
                employeeId,
                shopId
            };
        } else {
            params = {
                type: 'OFFLINE',
                orderId: orderid.generate(),
                employeeId,
                shopId
            };
        }
        let order = await OrderModel.create(params);
        if (!order) {
            return res.status(201).json({
                ok: false,
                message: 'failed to create order'
            });
        }
        console.log('create', eventItems);
        await OrderController.updateOrderItem({
            body: {
                orderId: order.orderId,
                merchandiseItems,
                eventItems
            }
        });

        const newOrder = await OrderModel.findOne({
            orderId: order.orderId
        })
            .populate('eventItems')
            .populate('merchandiseItems');

        // await calculateOrderSubtotal(newOrder)

        res.status(200).json({
            ok: true,
            data: newOrder
        });
    },
    createCentralMerchandiseOrder: async (req, res) => {
        const {
            cartData,
            totalPrice,
            totalPoints,
            tax,
            delivery,
            amount,
            shippingAddress,
            isPickup,
            shipment,
            deliveryOption,
            user
        } = req.body;
        let centralMerchandiseItems = [...cartData];
        let newOnlineOrder = new OrderModel({
            orderSubtotal: totalPrice,
            orderTax: tax,
            orderDelivery: delivery,
            orderAmount: amount,
            orderSubtotalPoint: totalPoints,
            user,
            orderId: orderid.generate(),
            centralMerchandiseItems,
            shippingAddress,
            isPickup,
            shipment,
            deliveryOption,
            type: 'ONLINE',
            status: 'PAID'
        });
        centralMerchandiseItems.map((item) => {
            MerchandiseService.onlineOrderInventoryLog(
                item.items._id,
                -item.quantity,
                newOnlineOrder.orderId
            );
        });
        await UserModel.updateOne(
            { _id: req._id },
            {
                $inc: {
                    balance: -parseFloat(amount).toFixed(2),
                    points: -parseInt(totalPoints)
                }
            }
        );
        const result = await newOnlineOrder.save();

        res.status(200).json({ result });
    },
    getAllCentralMerchandiseOrder: async (req, res) => {
        const orders = await OrderModel.find({
            type: 'ONLINE'
        })
            .populate('user')
            .populate('admin');

        if (!orders) {
            return res.status(201).json({
                message: 'error finding user orders'
            });
        }
        res.status(200).json(orders);
    },
    getAllCentralMerchandiseOrderByIdOrDate: async (req, res) => {
        let orderDateFrom;
        let orderDateTo;
        let orderId;
        let orders;

        if (req.body.orderId !== '') {
            orderId = req.body.orderId;
            orders = await OrderModel.find({
                orderId: orderId,
                type: 'ONLINE'
            })
                .populate('user')
                .populate('admin');
        } else if (req.body.orderDateFrom !== '' && req.body.orderDateTo !== '') {
            orderDateFrom = new Date(req.body.orderDateFrom);
            orderDateFrom.setHours(0, 0, 0, 0);
            orderDateTo = new Date(req.body.orderDateTo);
            orderDateTo.setHours(48, 0, 0, 0);
            orders = await OrderModel.find({
                orderDate: { $gte: orderDateFrom, $lte: orderDateTo },
                type: 'ONLINE'
            })
                .populate('user')
                .populate('admin');
        } else {
            orders = await OrderModel.find({
                type: 'ONLINE'
            })
                .populate('user')
                .populate('admin');
        }
        if (!orders) {
            return res.status(201).json({
                message: 'error finding user orders'
            });
        }
        res.status(200).json(orders);
    },
    getAllCentralMerchandiseShopOrder: async (req, res) => {
        const { pageNumber, pageSize } = req.query;

        const orders = await OrderModel.find({
            type: 'OFFLINE',
            shopId: req._id
        })
            .populate('user')
            .sort({ $natural: -1 })
            .skip((Number(pageNumber) - 1) * Number(pageSize))
            .limit(Number(pageSize))
            .lean();

        if (!orders[0])
            return res.status(404).json({
                message: 'No orders found!'
            }); // return and stop query totalCount

        const totalCount = await OrderModel.countDocuments({
            type: 'OFFLINE',
            shopId: req._id
        });

        res.status(200).json({ totalCount, orders });
    },
    updateCentralMerchandiseStatus: async (req, res) => {
        await OrderModel.updateOne(
            { _id: req.body._id },
            {
                $set: {
                    status: req.body.status
                }
            }
        );
        res.status(200).send('Update Success');
    },
    updateOrderUserId: async (req, res) => {
        await OrderModel.updateOne(
            { _id: req.body._id },
            {
                $set: {
                    user: req.body.userId
                }
            }
        );
        res.status(200).send('Update Success');
    },
    getOrdersByShopId: async (req, res) => {
        const orders = await OrderModel.find({
            shopId: req.params.id
        }).populate('user');

        if (!orders) {
            return res.status(201).json({
                ok: false,
                message: 'error finding user orders'
            });
        }
        res.status(201).json({
            ok: true,
            data: orders,
            count: orders.length
        });
    },
    updateOrderItem: async (req, res) => {
        const { orderId, eventItems, merchandiseItems } = req.body;

        let order = await OrderModel.findOne({
            orderId
        })
            .populate('user')
            .populate('eventItems')
            .populate('merchandiseItems');
        console.log('eventItems', eventItems);
        console.log('orderId', orderId);

        if (!order) {
            return res.status(201).json({
                ok: false,
                message: 'order not found'
            });
        }
        if (eventItems && eventItems.length > 0) {
            let event = await EventModel.findByIdAndUpdate(eventItems, {
                orderId: order.orderId
            }).lean();

            if (!event) {
                return res.status(201).json({
                    ok: false,
                    message: 'event not found or update to fail'
                });
            }

            order.orderEventItems[eventItems] = event;
            order.markModified('orderEventItems');
        }
        if (merchandiseItems) {
            const itemId = merchandiseItems.merchandiseId;
            const orderQuantity = merchandiseItems.orderQuantity;
            let merchandise = await MerchandiseModel.findById(itemId).lean();
            if (!merchandise) {
                return res.status(201).json({
                    ok: false,
                    message: 'merchandise not found or update to fail'
                });
            }
            const currentQuantity = order.orderMerchandiseItems[itemId]
                ? order.orderMerchandiseItems[itemId].orderQuantity
                : 0;

            merchandise.orderQuantity = currentQuantity + parseInt(orderQuantity);

            const itemTotal = merchandise.orderQuantity * parseFloat(merchandise.price);
            merchandise.merchandiseItemTotal = itemTotal.toFixed(2);
            order.orderMerchandiseItems[itemId] = merchandise;
            order.markModified('orderMerchandiseItems');
        }

        await order.save();
        // await calculateOrderSubtotal(order)
        if (res) {
            res.status(200).json({
                ok: true,
                data: order,
                message: 'action success'
            });
        }
    },
    addService: async (req, res) => {
        const { orderId, events } = req.body;

        let iscross = false;

        for (let i = 0; i < events.length; i++) {
            let event = events[i];
            let employeeSchedule = await UserService.getEmployeeScheduleByDay(
                event.employeeId,
                event.day
            );

            if (employeeSchedule.length > 0) {
                iscross = UserService.whetherCross(
                    event.start,
                    event.duration,
                    employeeSchedule[0].scheduleDoc
                );
            }
        }

        if (iscross) {
            res.status(200).json({ error: 'This time is not avaliable, Please select again!' });
            return;
        }
        let eventData = new EventModel(events[0]);

        let order = await OrderModel.findOne({
            orderId
        })
            .populate('user')
            .populate('eventItems')
            .populate('merchandiseItems');

        if (!order) {
            return res.status(201).json({
                ok: false,
                message: 'order not found'
            });
        }

        order.orderEventItems[eventData._id] = eventData;
        order.markModified('orderEventItems');

        await order.save();
        // await calculateOrderSubtotal(order)
        if (res) {
            res.status(200).json({
                ok: true,
                data: order,
                message: 'action success'
            });
        }
    },
    deleteAllOrders: async (req, res) => {
        await OrderModel.deleteMany();
        res.status(201).json({
            ok: true,
            message: 'all orders are deleted'
        });
    },
    deleteSingleOrder: async (req, res) => {
        const orderId = req.params.id;
        const order = await OrderModel.findOne({ orderId });
        if (!order)
            return res.status(201).json({
                ok: false,
                message: 'order not found'
            });

        order.remove();

        res.status(201).json({
            ok: true,
            message: 'order deleted'
        });
    },
    deleteOrderItems: async (req, res) => {
        const { orderId, eventItems, merchandiseItems } = req.body;
        let message = '';
        const order = await OrderModel.findOne({ orderId });
        if (!order) {
            return res.status(201).json({
                ok: false,
                message: 'order not found'
            });
        }
        if (eventItems && eventItems.length > 0) {
            let tempItems = JSON.parse(JSON.stringify(order.orderEventItems));
            for (key of eventItems) {
                delete tempItems[`${key}`];
            }
            order.orderEventItems = tempItems;
        }

        if (merchandiseItems && merchandiseItems.length > 0) {
            let tempItems = JSON.parse(JSON.stringify(order.orderMerchandiseItems));
            //if only one item was passed
            if (merchandiseItems.length == 1 && !tempItems[merchandiseItems[0]]) {
                return res.status(201).json({
                    ok: false,
                    data: order,
                    message: 'item does not belong to order'
                });
            }
            for (key of merchandiseItems) {
                delete tempItems[key];
            }
            order.orderMerchandiseItems = tempItems;
            message = 'merchandise removed';
        }

        await order.save();
        // await calculateOrderSubtotal(order)

        res.status(201).json({
            ok: true,
            data: order,
            message
        });
    }
};

module.exports = OrderController;
