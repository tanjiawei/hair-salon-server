const UserModel = require('../Models/UserModel');
const ReservationModel = require('../Models/ReservationModel');
const FindPassWordModel = require('../Models/FindPasswordModel.js');

const JWT = require('jsonwebtoken');

const AdminService = require('../Service/AdminService');
const EmployeeService = require('../Service/EmployeeService');
const HRServiceService = require('../Service/HRServiceService');
const UserService = require('../Service/UserService');
const scheduleModel = require('../Models/ScheduleModel');
const HRServiceBondModel = require('../Models/HRServiceBondModel');
const MessagerModel = require('../Models/MessagerModel');
const MessagerService = require('../Service/MessagerService');

const mongoose = require('mongoose');
const EventModel = require('../Models/EventModel');
const nodemailer = require('nodemailer');
const orderid = require('order-id')('mysecret');

const Moment = require('moment');
const fs = require('fs');
const ReloadModel = require('../Models/ReloadModel');
const PointsModel = require('../Models/PointsModel');
const OrderModel = require('../Models/OrderModel');
const AppSettingModel = require('../Models/AppSettingModel');

const ServiceMerchandiseBondModel = require('../Models/ServiceMerchandiseBond');
const { MerchandiseService } = require('../Service/MerchandiseService');

const CanadaPostClient = require('canadapost-api');
const cpc = new CanadaPostClient('c11f7d9ae4906494', 'ab0544a362487ff393cf14', '0009602928');
const Auth = require('../Service/AuthService');

const UserController = {
    login: async (req, res) => {
        let doc = null;
        if (req.body.email) {
            doc = await UserModel.find({ email: req.body.email })
                .select('+password +passwordSalt')
                .exec();
            if (doc.length === 0) {
                res.status(400).json({ error: 'Not a registered user' });
                return;
            }
        } else if (req.body.phone) {
            doc = await UserModel.find({ phone: req.body.phone })
                .select('+password +passwordSalt')
                .exec();
            if (doc.length === 0) {
                res.status(400).json({ error: 'Not a registered user' });
                return;
            }
        }

        const user = doc[0];
        if (await Auth.checkPassword(req.body.password, user.password)) {
            const data = user.toObject();
            const jwt = JWT.sign(data._id.toString(), process.env.USER_JWT_PRIVATE_KEY);

            res.status(200).json({ data, jwt });
        } else {
            res.status(400).json({ error: 'Password missmatch' });
        }
    },

    loginWithJWT: async (req, res) => {
        const userData = await UserModel.findById(req._id);
        res.json({ data: userData });
    },

    register: async (req, res) => {
        // check if email is in use
        // if not then add new user into data base

        if (req.body.email) {
            const doc = await UserModel.find({ email: req.body.email });
            if (doc.length > 0) {
                res.status(400).json({ error: 'Email already in use' });
                return;
            }
        }
        if (req.body.phone) {
            const docPhone = await UserModel.find({ phone: req.body.phone });
            if (docPhone.length > 0) return res.status(400).json({ error: 'Phone already in use' });
        }

        const data = new UserModel({
            email: req.body.email,
            name: req.body.name,
            newsLetter: req.body.newsLetter,
            phone: req.body.phone,
            passwordSalt: salt,
            password: await Auth.encryptPassword(req.body.password)
        });

        data.save()
            .then((document) => {
                const data = document.toObject();
                const jwt = JWT.sign(data._id.toString(), process.env.USER_JWT_PRIVATE_KEY);
                res.status(200).json({
                    data,
                    jwt
                });
            })
            .catch((err) => {
                res.status(500).json('Internal Server Error');
            });
    },
    makeReservation: async (req, res) => {
        const { events } = req.body;

        let iscross = false;

        for (let i = 0; i < events.length; i++) {
            let event = events[i];
            let employeeSchedule = await UserService.getEmployeeScheduleByDay(
                event.employeeId,
                event.day
            );
            if (employeeSchedule.length > 0) {
                let scheduleDoc = employeeSchedule[0].scheduleDoc;
                for (let j = 0; j < scheduleDoc.length; j++) {
                    scheduleDoc[j];
                }
                iscross = UserService.whetherCross(
                    event.start,
                    event.duration,
                    employeeSchedule[0].scheduleDoc
                );
            }
        }

        if (iscross) {
            return res.status(200).json({
                error: 'This time is not avaliable, Please select again!'
            });
        }
        const id = orderid.generate();
        const newEvents = await EventModel.insertMany(events);
        let result = null;
        if (req.body.type == 'user') {
            result = new ReservationModel({
                orderId: id,
                userId: req.body.userId,
                shopId: req.body.shopId,
                events: newEvents,
                type: req.body.type,
                userInfo: req.body.userInfo
            });
        } else if (req.body.type == 'shop') {
            result = new ReservationModel({
                orderId: id,
                shopId: req.body.shopId,
                events: newEvents,
                type: req.body.type
            });
        } else {
            res.status(200).json({ error: 'No type!' });
            return;
        }

        for (let i = 0; i < newEvents.length; i++) {
            let event = newEvents[i];
            let content =
                'Name: ' +
                event.username +
                '\nService: ' +
                event.selectedService.name +
                '\nTime: ' +
                Moment(event.start, 'America/Toronto').format('ll  HH:mm ~ ') +
                Moment(event.end, 'America/Toronto').format('HH:mm');
            let messager = {
                shopId: event.shopId,
                message: 'You have an appointment \n' + content,
                userId: event.userId
            };
            let title = 'You have an appointment!';
            let body = content;
            let data = {
                type: 'MakeAppointment',
                service: event.selectedService.name,
                name: event.username,
                start: event.start.getTime() + '',
                end: event.end.getTime() + ''
            };
            MessagerService.sendMessageToEmployee(messager, event.employeeId, title, body, data);
        }

        result.save().then((document) => {
            console.log(document);
            res.status(200).json('Success!');
        });
    },

    getAllMerchants: async (req, res) => {
        const merchants = await AdminService.getAllMerchants();
        if (merchants === null) {
            res.status(500).json({ error: 'Internal Server Error' });
        }

        res.status(200).json(merchants);
    },

    getEmployeeByIDList: async (req, res) => {
        const employees = await EmployeeService.findAllEmployeeByIDList(req.body.idList);
        if (employees === null) {
            res.status(500).json({ error: 'Internal Server Error' });
        }

        res.status(200).json(employees);
    },

    /**
     * The function will return the employee and his/her working days and his/her scheduled services
     * at that day in the next four week with the given Employee id.
     */
    getEmployeeScheduleById: async (req, res) => {
        let employeeid = req.params.id;
        let currentDate = new Date();
        currentDate.setHours(0, 0, 0, 0);
        // ymd: { $dateToString: { format: "%Y-%m-%d", date: "$day" } },

        const scheduleList = await scheduleModel.aggregate([
            {
                $match: {
                    $and: [
                        { employeeid: mongoose.Types.ObjectId(employeeid) },
                        { day: { $gt: currentDate } }
                    ]
                }
            },
            {
                $project: {
                    day: 1,
                    startTime: 1,
                    endTime: 1,
                    employeeid: 1
                }
            },
            {
                $lookup: {
                    from: 'events',
                    let: { eId: '$employeeid', day: '$day' },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ['$employeeId', '$$eId'] },
                                        { $eq: ['$day', '$$day'] },
                                        { $ne: ['$status', 'canceled'] }
                                    ]
                                }
                            }
                        },
                        {
                            $project: {
                                start: 1,
                                duration: 1,
                                end: 1
                            }
                        }
                    ],
                    as: 'scheduleDoc'
                }
            }
        ]);
        res.json(scheduleList);
    },

    /**
     * Send a verification email to the user for password restore with the given Id.
     */
    sendVerifyEmailCode: async (req, res) => {
        let doc = await UserModel.find({ email: req.body.email });
        if (doc.length === 0) {
            res.status(400).send({ error: "Email doesn't exist" });
            return;
        }

        let verifyCode = Math.floor(Math.random() * 8999 + 1000);

        // Store the verifyCode
        let newEntry = new FindPassWordModel({
            userId: doc[0]._id,
            tmpCode: verifyCode
        });

        newEntry.save();

        // Build transport
        const transporter = nodemailer.createTransport({
            host: 'smtp-mail.outlook.com',
            secureConnection: false,
            port: 587,
            tls: {
                ciphers: 'SSLv3'
            },
            auth: {
                user: 'westley.tan@outlook.com',
                pass: process.env.pass || 'notapassword'
            }
        });

        await transporter.sendMail({
            from: 'westley.tan@outlook.com',
            to: `${req.body.email}`,
            subject: 'Varification Code',
            text: `${verifyCode}`
        });
    },

    restoreUserPassword: async (req, res) => {
        let { email, verifyCode, newPassword } = req.body;
        let data = await UserModel.find({ email: email });
        let codeInfo = await FindPassWordModel.find({ userId: data[0]._id });
        if (codeInfo.length < 1) {
            res.status(400).json({ error: 'Please get verify code!' });
            return;
        }
        if (verifyCode === codeInfo[0].tmpCode) {
            // Update new password

            let result = await UserModel.updateOne(
                { _id: data[0]._id },
                {
                    $set: {
                        passwordSalt: salt,
                        password: await Auth.encryptPassword(newPassword)
                    }
                }
            );

            // Remove verifyCode
            await FindPassWordModel.deleteOne({ _id: codeInfo[0]._id });
            if (result.ok) {
                res.status(200).json('Reset password success!');
            } else {
                res.status(400).json({ error: 'Reset password success!' });
            }
        } else {
            res.status(400).json({ error: 'Internal Serve Error' });
        }
    },

    getAllServices: async (req, res) => {
        const services = await HRServiceService.getAllServices();
        if (services === null) {
            res.status(500).json({ error: 'Internal Serve Error' });
        } else {
            res.status(200).json(services);
        }
    },
    getAllServicesByAdminID: async (req, res) => {
        const services = await HRServiceService.getAllServicesByID(req.query.id);
        if (services === null) {
            res.status(500).json({ error: 'Internal Serve Error' });
        } else {
            res.status(200).json(services);
        }
    },
    getEmployeeByService: async (req, res) => {
        const servicebond = await HRServiceBondModel.aggregate([
            {
                $match: {
                    serviceId: mongoose.Types.ObjectId(req.query.serviceid)
                }
            },
            {
                $lookup: {
                    from: 'employees',
                    localField: 'employeeId',
                    foreignField: '_id',
                    as: 'employee'
                }
            },
            {
                $project: {
                    employee: {
                        _id: 1,
                        name: 1,
                        email: 1,
                        phone: 1
                    },
                    price: 1
                }
            }
        ]);
        // const services = await EmployeeService.getEmployeesByService(req.query.serviceid,req.query)
        if (servicebond === null) {
            res.status(500).json({ error: 'Internal Serve Error' });
        } else {
            res.status(200).json(servicebond);
        }
    },

    getEmployeeByServiceAndTime: async (req, res) => {
        let tmpStart = new Date(req.query.start);
        tmpStart.setHours(0, 0, 0, 0);
        let tmpEnd = new Date(req.query.end);
        tmpEnd.setHours(24, 0, 0, 0);

        const servicebond = await HRServiceBondModel.aggregate([
            {
                $match: {
                    serviceId: mongoose.Types.ObjectId(req.query.serviceid)
                }
            },
            {
                $lookup: {
                    from: 'employees',
                    localField: 'employeeId',
                    foreignField: '_id',
                    as: 'employee'
                }
            },
            {
                $lookup: {
                    from: 'schedules',
                    let: { employee_id: '$employeeId' },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        {
                                            $eq: ['$employeeid', '$$employee_id']
                                        },
                                        { $gte: ['$day', tmpStart] },
                                        { $lte: ['$day', tmpEnd] }
                                    ]
                                }
                            }
                        }
                    ],
                    as: 'schedules'
                }
            },
            {
                $unwind: '$schedules'
            },
            {
                $lookup: {
                    from: 'events',
                    let: {
                        eId: '$schedules.employeeid',
                        day: '$schedules.day'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ['$employeeId', '$$eId'] },
                                        { $eq: ['$day', '$$day'] },
                                        { $ne: ['$status', 'canceled'] }
                                    ]
                                }
                            }
                        }
                    ],
                    as: 'scheduleDoc'
                }
            }
        ]);

        servicebond.forEach((data) => {
            delete data.employee[0].password;
            delete data.employee[0].passwordSalt;
        });
        // const services = await EmployeeService.getEmployeesByService(req.query.serviceid,req.query)
        if (servicebond === null) {
            res.status(500).json({ error: 'Internal Serve Error' });
        } else {
            res.status(200).json(servicebond);
        }
    },

    editProfile: async (req, res) => {
        _id = req._id ? req._id : req.body._id;
        const user = await UserModel.findById(_id);
        if (!user) return res.status(404).send(null);

        if (req.body.imgURI) {
            req.body.profileImage = req.body.imgURI[0];
            fs.unlink(user.profileImage, (err) => (!err ? null : console.error(err)));
        }
        const doc = await UserService.editProfile(_id, req.body);
        res.json(doc);
    },

    addPoints: async (req, res) => {
        let doc = await UserModel.updateOne(
            { _id: req.body._id },
            {
                $inc: {
                    points: parseInt(req.body.points)
                }
            }
        );
        if (doc.ok) {
            res.status(200).json({ ok: true });
        } else {
            res.status(200).json({ ok: false });
        }
    },

    getEmployeeAvailableTime: async (req, res) => {
        const { id, timeStamp } = req.query;

        const qry = {
            employeeId: id,
            day: { $eq: timeStamp }
        };

        const events = await EventModel.find(qry);
        res.json(events);
    },

    getMyReservation: async (req, res) => {
        // const docs = await ReservationModel.find({ userId: req._id }).sort({ "_id": -1 })
        let doc = await ReservationModel.aggregate([
            {
                $match: { userId: mongoose.Types.ObjectId(req._id) }
            },
            {
                $lookup: {
                    from: 'events',
                    localField: 'events._id',
                    foreignField: '_id',
                    as: 'eventDetail'
                }
            },
            { $sort: { _id: -1 } }
        ]);
        if (doc === null) {
            res.status(200).json({ error: 'Internal Serve Error' });
        } else {
            res.status(200).json(doc);
        }
    },

    postCheckin: async (req, res) => {
        let result = await EventModel.findById(req.body.id);

        let doc = null;
        if (!result.status) {
            doc = await EventModel.updateOne(
                { _id: req.body.id },
                {
                    $set: {
                        status: 'arrived'
                    }
                }
            );
        }

        await MessagerService.sendMessageToEmployee(
            {
                type: 'employee',
                employeeId: result.employeeId,
                message:
                    result.username +
                    ' has arrived!\nService: ' +
                    result.selectedService.name +
                    '\nTime: ' +
                    Moment(result.start).format('ll  HH:mm ~ ') +
                    Moment(result.end).format('HH:mm'),
                userId: req.body.id
            },
            result.employeeId,
            'Message from ' + result.username,
            result.username + ' has arrived!',
            {
                type: 'CheckIn',
                name: result.username,
                service: result.selectedService.name,
                time:
                    Moment(result.start).format('ll  HH:mm ~ ') + Moment(result.end).format('HH:mm')
            }
        );

        if (doc && doc.ok) {
            res.status(200).json({ message: 'Check in success!' });
        } else {
            res.status(400).json({ error: 'Check in failed!' });
        }
    },

    getUserById: async (req, res) => {
        const doc = await UserModel.find({
            _id: mongoose.Types.ObjectId(req.body._id)
        })
            .sort({ name: 1 })
            .skip(0)
            .limit(0);
        res.json(doc);
    },

    getAllUsers: async (req, res) => {
        let doc = await UserModel.find({}).select({
            email: 1,
            name: 1,
            balance: 1,
            points: 1,
            newsLetter: 1,
            phone: 1
        });
        res.json(doc);
    },

    getUserByEmailOrPhone: async (req, res) => {
        let doc;
        if (req.body.email !== '') {
            doc = await UserModel.find({ email: req.body.email });
        } else if (req.body.phone !== '') {
            doc = await UserModel.find({ phone: req.body.phone });
        }
        res.json(doc);
    },

    postAddUserAddress: async (req, res) => {
        const newId = new mongoose.Types.ObjectId();
        let address = [];
        address.push({
            _id: newId,
            street: req.body.address.street,
            city: req.body.address.city,
            country: req.body.address.country,
            state: req.body.address.state,
            postalcode: req.body.address.postalcode,
            telephone: req.body.address.telephone,
            isDefault: req.body.address.isDefault
        });

        if (address[0].isDefault) {
            const doc1 = await UserModel.update(
                {
                    _id: mongoose.Types.ObjectId(req.body._id),
                    'address.isDefault': true
                },
                { $set: { 'address.$.isDefault': false } }
            );
            const doc2 = await UserModel.findOneAndUpdate(
                { _id: mongoose.Types.ObjectId(req.body._id) },
                { $push: { address: address } }
            );
            res.status(200).json({ message: 'Success add address' });
        } else {
            const doc1 = await UserModel.find({
                _id: mongoose.Types.ObjectId(req.body._id),
                'address.isDefault': true
            });
            if (doc1.length === 0) address[0].isDefault = true;
            const doc2 = await UserModel.findOneAndUpdate(
                { _id: mongoose.Types.ObjectId(req.body._id) },
                { $push: { address: address } }
            );
            res.status(200).json({ message: 'Success add address' });
        }
    },

    postEditUserAddress: async (req, res) => {
        if (req.body.address.isDefault) {
            const doc1 = await UserModel.update(
                {
                    _id: mongoose.Types.ObjectId(req.body._id),
                    'address.isDefault': true
                },
                { $set: { 'address.$.isDefault': false } }
            );
            const doc2 = await UserModel.update(
                {
                    _id: mongoose.Types.ObjectId(req.body._id),
                    'address._id': mongoose.Types.ObjectId(req.body.address._id)
                },
                {
                    $set: {
                        'address.$.street': req.body.address.street,
                        'address.$.city': req.body.address.city,
                        'address.$.country': req.body.address.country,
                        'address.$.state': req.body.address.state,
                        'address.$.postalcode': req.body.address.postalcode,
                        'address.$.telephone': req.body.address.telephone,
                        'address.$.isDefault': req.body.address.isDefault
                    }
                }
            );
            res.status(200).json({ message: 'Success add address' });
        } else {
            const doc1 = await UserModel.find({
                _id: mongoose.Types.ObjectId(req.body._id),
                'address.isDefault': true
            });
            if (doc1.length === 0) {
                req.body.address.isDefault = true;
            } else {
                for (let i = 0; i < doc1[0]._doc.address.length; i++) {
                    if (doc1[0]._doc.address[i]._id.toString() === req.body.address._id) {
                        if (doc1[0]._doc.address[i].isDefault) {
                            req.body.address.isDefault = true;
                            break;
                        }
                    }
                }
            }
            if (doc1.length === 0) req.body.address.isDefault = true;
            const doc2 = await UserModel.update(
                {
                    _id: mongoose.Types.ObjectId(req.body._id),
                    'address._id': mongoose.Types.ObjectId(req.body.address._id)
                },
                {
                    $set: {
                        'address.$.street': req.body.address.street,
                        'address.$.city': req.body.address.city,
                        'address.$.country': req.body.address.country,
                        'address.$.state': req.body.address.state,
                        'address.$.postalcode': req.body.address.postalcode,
                        'address.$.telephone': req.body.address.telephone,
                        'address.$.isDefault': req.body.address.isDefault
                    }
                }
            );
            res.status(200).json({ message: 'Success add address' });
        }
    },

    getMessages: async (req, res) => {
        let result = await MessagerModel.aggregate([
            {
                $match: {
                    type: 'user',
                    userId: mongoose.Types.ObjectId(req._id)
                }
            }
            // {
            //     $lookup: {
            //         from: "employees",
            //         localField: "employeeId",
            //         foreignField: "_id",
            //         as: "employeeDetail"
            //     }
            // }
        ])
            .sort({ createTime: -1 })
            .limit(20);

        if (result) {
            res.status(200).json(result);
        } else {
            res.status(400).json({ error: 'Internal Serve Error!' });
        }
    },

    bindFirebaseToken: async (req, res) => {
        let result = await UserModel.updateOne(
            { _id: req.body.id },
            { $addToSet: { tokens: req.body.token } }
        );
        let docs = await UserModel.findById(req.body.id);
        if (docs.tokens.length > 10) {
            docs.tokens.shift();
            await UserModel.updateOne({ _id: req.body.id }, { $set: { tokens: docs.tokens } });
        }
        if (result && result.ok) {
            res.status(200).json({ message: 'success!' });
        } else {
            res.status(400).json({ error: 'failed!' });
        }
    },

    createReload: async (req, res) => {
        let result = new ReloadModel({
            value: req.body.value,
            userId: req._id,
            bonus: req.body.bonus ? req.body.bonus : ''
        });
        result.save().then((doc) => {
            res.status(200).json({ ok: true, data: doc });
        });
    },

    updateReload: async (req, res) => {
        let status = 'paid';
        if (req.body.message.indexOf('APPROVED') == -1) {
            status = 'failed';
        }
        ReloadModel.updateOne(
            {
                _id: req.body.id
            },
            {
                $set: {
                    paymentDetail: req.body.data,
                    status: status
                }
            }
        );

        if (status == 'paid') {
            let reload = await ReloadModel.findById(req.body.id);
            let tmpValue = parseFloat(reload.value) + parseFloat(reload.bonus ? reload.bonus : 0);

            let user = await UserModel.updateOne(
                {
                    _id: req._id
                },
                {
                    $inc: {
                        balance: tmpValue.toFixed(2)
                    }
                }
            );
            if (user.ok) {
                res.status(200).json({ ok: true });
            } else {
                res.status(200).json({ ok: false });
            }
        } else {
            res.status(200).json({ ok: false });
        }
    },

    purchaseOrder: async (req, res) => {
        let orderDetail = await OrderModel.find({ orderId: req.body.orderId });
        let orderEventItems = orderDetail[0].orderEventItems;
        let orderMerchandiseItems = orderDetail[0].orderMerchandiseItems;

        let serviceFees = 0;
        Object.keys(orderEventItems).forEach((key) => {
            ServiceMerchandiseBondModel.findOne({
                serviceId: orderEventItems[key].serviceId
            }).then((res) => {
                MerchandiseService.shopOrderInventoryLog(
                    res.merchandiseId,
                    orderDetail[0].shopId,
                    res.usage,
                    orderDetail[0].employeeId,
                    req.body.orderId,
                    'service'
                );
            });
            serviceFees += parseInt(orderEventItems[key].selectedService.price);
        });
        Object.keys(orderMerchandiseItems).forEach((key) => {
            MerchandiseService.shopOrderInventoryLog(
                orderMerchandiseItems[key]._id,
                orderDetail[0].shopId,
                -orderMerchandiseItems[key].orderQuantity,
                orderDetail[0].employeeId,
                req.body.orderId
            );
        });

        let pointsRule = await AppSettingModel.find({ type: 'Points' });
        let value = 0;
        for (let i = 0; i < pointsRule[0].content.length; i++) {
            if (pointsRule[0].content[i].key == 'service') {
                value = parseInt(pointsRule[0].content[i].value);
                break;
            }
        }
        let points = parseInt(serviceFees / value);
        if (points > 0) {
            let pointModel = new PointsModel({
                value: points,
                userId: req._id,
                type: 'service'
            });
            pointModel.save();
        }
        let user = await UserModel.updateOne(
            { _id: req._id },
            {
                $inc: {
                    balance: -parseFloat(req.body.price).toFixed(2),
                    points: points
                }
            }
        );
        if (user.ok) {
            let order = await OrderModel.updateOne(
                { orderId: req.body.orderId },
                {
                    $set: {
                        status: 'PAID',
                        tipsAmount: parseFloat(req.body.tips).toFixed(2),
                        orderAmount: parseFloat(req.body.price).toFixed(2),
                        tax: parseFloat(req.body.tax).toFixed(2),
                        orderSubtotal: parseFloat(req.body.orderSubtotal).toFixed(2)
                    }
                }
            );
            if (order.ok) {
                let user = await UserModel.findById(req._id);
                res.status(200).json({ ok: true, user, points });
            } else {
                res.status(200).json({ ok: false });
            }
        } else {
            res.status(200).json({ ok: false });
        }
    },

    addFeedingPoint: async (req, res) => {
        let tmpStart = new Date();
        tmpStart.setHours(0, 0, 0, 0);
        let tmpEnd = new Date();
        tmpEnd.setHours(24, 0, 0, 0);
        let userPoints = await PointsModel.find({
            userId: mongoose.Types.ObjectId(req.body._id),
            type: 'share',
            createdAt: { $gte: tmpStart, $lte: tmpEnd }
        });
        if (userPoints.length < 1) {
            let pointsRule = await AppSettingModel.find({ type: 'Points' });
            let value = 0;
            for (let i = 0; i < pointsRule[0].content.length; i++) {
                if (pointsRule[0].content[i].key == 'share') {
                    value = parseInt(pointsRule[0].content[i].value);
                    break;
                }
            }
            let points = parseInt(value);
            if (points > 0) {
                let pointModel = new PointsModel({
                    value: points,
                    userId: req._id,
                    type: 'share'
                });
                pointModel
                    .save()
                    .then(async () => {
                        let user = await UserModel.updateOne(
                            { _id: req._id },
                            {
                                $inc: {
                                    points: points
                                }
                            }
                        );
                        res.status(200).send({
                            status: 'ok',
                            message: 'Earned ' + points + ' points!'
                        });
                    })
                    .catch((err) => {
                        console.log(err);
                        res.status(500).send('Internal Server Error (' + err + ')');
                    });
            }
        } else {
            res.status(200).send({ status: 'no', message: 'Today already added' });
        }
    },

    getCanadaPost: async (req, res) => {
        const rateQuery = {
            parcelCharacteristics: {
                weight: req.query.weight
            },
            originPostalCode: 'K2J4N5',
            destination: {
                domestic: {
                    postalCode: req.query.postalcode
                }
            }
        };

        cpc.getRates(rateQuery)
            .then((result) => {
                console.log(result);
                res.status(200).json({ ok: true, result });
            })
            .catch((err) => {
                console.log(err);
                res.status(200).json({ ok: false, error: 'Internal Serve Error' });
            });
    },

    postCreateNCSCanadaPost: async (req, res) => {
        let shipment = req.body;
        cpc.createNonContractShipment(shipment)
            .then((result) => {
                console.log(result);
                res.status(200).json({ ok: true, result });
            })
            .catch((err) => {
                console.log(err);
                res.status(200).json({ ok: false, error: 'Internal Serve Error' });
            });
    }
};

module.exports = UserController;
