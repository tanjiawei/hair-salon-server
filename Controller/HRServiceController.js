const express = require('express');
const HRServicesModel = require('../Models/HRServiceModel');
const HRServiceService = require('../Service/HRServiceService');
const HRServiceBondModel = require('../Models/HRServiceBondModel');
var mongoose = require('mongoose');
const ServiceMerchandiseBondModel = require('../Models/ServiceMerchandiseBond');

const HRServiceController = {
    addService: async (req, res) => {
        const data = new HRServicesModel({
            name: req.body.name,
            duration: req.body.duration,
            price: req.body.price,
            description: req.body.description,
            adminid: req._id,
            categoryId: req.body.categoryId,
            roomId: req.body.roomId
        });
        data.save()
            .then(() => {
                res.status(200).send('Save Success');
            })
            .catch((err) => {
                console.log(err);
                res.status(500).send('Internal Server Error (' + err + ')');
            });
    },

    updateService: async (req, res) => {
        const servicesDocument = await HRServicesModel.updateOne(
            { _id: req.body._id },
            {
                $set: {
                    name: req.body.name,
                    duration: req.body.duration,
                    price: req.body.price,
                    description: req.body.description,
                    adminid: req._id,
                    categoryId: req.body.categoryId,
                    roomId: req.body.roomId
                }
            }
        );
        res.status(200).send('Update Scucess');
    },

    deleteService: async (req, res) => {
        const servicesDocument = await HRServicesModel.deleteOne({ _id: req.body._id });
        res.status(200).send('Delete Scucess');
    },

    getAllService: async (req, res) => {
        const serviceList = await HRServiceService.getAllServicesByID(req.query.id);
        res.json(serviceList);
    },

    getAllServicesCategory: async (req, res) => {
        const serviceList = await HRServicesModel.aggregate([
            {
                $match: {
                    adminid: mongoose.Types.ObjectId(req.query.id)
                }
            },
            {
                $group: {
                    _id: '$categoryId',
                    children: { $push: { title: '$name', key: '$_id', price: '$price' } }
                }
            },
            {
                $lookup: {
                    from: 'hrservicecategories',
                    localField: '_id',
                    foreignField: '_id',
                    as: 'docCategory'
                }
            }
        ]);
        res.json(serviceList);
    },

    addServiceBond: async (req, res) => {
        const servicesDocument = await HRServiceBondModel.deleteMany({
            employeeId: mongoose.Types.ObjectId(req.body.employeeId)
        });
        const data = new HRServiceBondModel({
            employeeId: req.body.employeeId,
            serviceId: req.body.serviceId,
            price: req.body.price
        });
        data.save().then(() => {
            res.status(200).send('Save Success');
        });
    },

    deleteServiceBond: async (req, res) => {
        const servicesDocument = await HRServiceBondModel.deleteMany({
            employeeId: mongoose.Types.ObjectId(req.body.employeeId)
        });
        res.status(200).send('Delete Scucess');
    },

    getCheckedServiceBond: async (req, res) => {
        const doc = await HRServiceBondModel.find({
            employeeId: mongoose.Types.ObjectId(req.body.employeeId)
        })
            .skip(0)
            .limit(0);
        res.json(doc);
    },

    getServiceMerchandiseBonds: async (req, res) => {
        let bonds = await ServiceMerchandiseBondModel.find({ serviceId: req.params.serviceId });
        res.json(bonds);
    },

    addServiceMerchandiseBonds: async (req, res) => {
        serviceId = req.body.serviceId;
        bondList = req.body.bondList;
        bondList.forEach((bond) => {
            let new_bond = new ServiceMerchandiseBondModel({
                serviceId: serviceId,
                ...bond
            });
            new_bond.save();
        });
        res.status(200).send('Save Scucess');
    },

    removeServiceMerchandiseBonds: async (req, res) => {
        serviceId = req.params.serviceId;
        await ServiceMerchandiseBondModel.deleteMany({
            serviceId: mongoose.Types.ObjectId(serviceId)
        });
        res.status(200).send('remove old bond');
    }
};

module.exports = HRServiceController;
