const HRServiceCategoryModel = require('../Models/HRServiceCategoryModel.js');
const Mongoose = require('mongoose');

const HRServiceCategoryController = {
    // Fetch all the exist HRService Category
    getAllCategories: async (req, res) => {
        let categoryList = await HRServiceCategoryModel.find({
            adminid: Mongoose.Types.ObjectId(req.query.id)
        });
        res.json(categoryList);
    },

    // Add new HRService category by name.
    addCategory: async (req, res) => {
        let newEntry = new HRServiceCategoryModel({
            name: req.body.name,
            adminid: req.body.adminId
        });

        const result = await newEntry.save();

        res.status(200).send(result);
    },

    // Update an exist category.
    updateCategory: async (req, res) => {
        const result = await HRServiceCategoryModel.updateOne(
            { _id: req.body._id },
            {
                $set: {
                    name: req.body.name
                }
            }
        );
        res.status(200).send(result);
    },

    // Delete category by _id
    deleteCategory: async (req, res) => {
        let _id = req.params._id;
        await HRServiceCategoryModel.deleteOne({ _id: Mongoose.Types.ObjectId(_id) });

        res.status(200).send('Delete Success');
    }
};

module.exports = HRServiceCategoryController;
