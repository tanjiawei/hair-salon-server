const CentralInventoryLogModel = require('../Models/CentralInventoryLogModel');
const CentralMerchandiseModel = require('../Models/CentralMerchandiseModel');
const httpMsg = require('../Util/httpCustomMessage');
const Fawn = require('fawn');

const CentralInventoryLogController = {
    getAllInventoryLogs: async (req, res) => {
        const { pageNumber, pageSize } = req.query;
        const logs = await CentralInventoryLogModel.find()
            .populate('merchandise')
            .sort({ dateIn: -1 })
            .skip((Number(pageNumber) - 1) * Number(pageSize))
            .limit(Number(pageSize))
            .lean();

        if (!logs[0]) return res.status(404).send(httpMsg.M400); // return and stop query totalCount

        const totalCount = await CentralInventoryLogModel.countDocuments({});

        res.json({ totalCount, logs });
    },

    addInventoryLog: async (req, res) => {
        const { merchandise, stockLog, note } = req.body;

        if (!merchandise || !stockLog) return res.status(400).send(httpMsg.M400);

        const result = await CentralMerchandiseModel.findById(merchandise);
        if (!result) return res.status(404).send(httpMsg.M404);

        if (result.stock + Number(stockLog) < 0) return res.status(400).send(httpMsg.M400);

        const newEntry = new CentralInventoryLogModel({
            merchandise,
            stockLog,
            note,
            totalStock: Number(result.stock) + Number(stockLog)
        });

        new Fawn.Task()
            .save('centralinventorylogs', newEntry)
            .update(
                'centralmerchandises',
                { _id: result._id },
                {
                    $inc: {
                        stock: +Number(stockLog)
                    },
                    $set: {
                        isValid: true
                    }
                }
            )
            .run();

        res.send(newEntry);
    }
};

module.exports = CentralInventoryLogController;
