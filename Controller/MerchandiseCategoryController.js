const mongoose = require('mongoose');
const MerchandiseCategoryModel = require('../Models/MerchandiseCategoryModel');
const httpMsg = require('../Util/httpCustomMessage');
const fs = require('fs');

const merchandiseCategoryController = {
    addCategory: async (req, res) => {
        const imgLink = req.file ? req.file.destination + req.file.filename : '';

        const data = new MerchandiseCategoryModel({
            name: req.body.name,
            adminid: req.body.adminid,
            imgLink
        });

        const result = await data.save();

        res.status(200).send(result);
    },

    updateCategory: async (req, res) => {
        const { _id, name } = req.body;

        const { name: oldName, imgLink: oldLink } = await MerchandiseCategoryModel.findById(_id);
        if (!oldName) return res.status(404).send(httpMsg.M404);

        const imgLink = req.file ? req.file.destination + req.file.filename : oldLink;
        const result = await MerchandiseCategoryModel.findByIdAndUpdate(
            _id,
            {
                $set: { name, imgLink }
            },
            { new: true }
        );

        if (req.file && oldLink) fs.unlink(oldLink, (err) => console.error(err));

        res.status(200).send(result);
    },

    deleteCategory: async (req, res) => {
        const category = await MerchandiseCategoryModel.findById(req.params._id);
        if (!category) return res.status(404).send(httpMsg.M404);

        fs.unlink(category.imgLink, (err) => console.error(err));

        await MerchandiseCategoryModel.deleteOne({
            _id: mongoose.Types.ObjectId(req.params._id)
        });
        res.status(200).send(httpMsg.M200);
    },

    getAllCategory: async (req, res) => {
        const categoryList = await MerchandiseCategoryModel.find({
            adminid: mongoose.Types.ObjectId(req.query.id)
        });
        res.json(categoryList);
    }
};

module.exports = merchandiseCategoryController;
