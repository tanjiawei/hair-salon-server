const CentralMerchandiseCategoryModel = require('../Models/CentralMerchandiseCategoryModel')
const fs = require('fs')
const httpMsg = require('../Util/httpCustomMessage')

const centralMerchandiseCategoryController = {
    addCategory: async (req, res) => {
        if (!req.body.name) return res.status(400).send(httpMsg.M400)

        const imgLink = req.file ? req.file.destination + req.file.filename : ''

        const category = new CentralMerchandiseCategoryModel({
            name: req.body.name,
            imgLink
        })

        await category.save()

        res.send(category)
    },

    updateCategory: async (req, res) => {
        const { name } = req.body

        const { name: oldName, imgLink: oldLink } = await CentralMerchandiseCategoryModel.findById(
            req.params._id
        )

        if (!oldName) return res.status(404).send(httpMsg.M404)

        const imgLink = req.file ? req.file.destination + req.file.filename : oldLink

        const category = await CentralMerchandiseCategoryModel.findByIdAndUpdate(
            req.params._id,
            {
                $set: {
                    name,
                    imgLink
                }
            },
            { new: true }
        )

        if (req.file && oldLink && oldLink.length > 0)
            fs.unlink(oldLink, (err) => console.error(err))

        res.status(200).send(category)
    },

    deleteCategory: async (req, res) => {
        const category = await CentralMerchandiseCategoryModel.findById(req.params._id)
        if (!category) return res.status(404).send(httpMsg.M404)

        fs.unlink(category.imgLink, (err) => {
            console.error(err)
        })

        await CentralMerchandiseCategoryModel.deleteOne({
            _id: req.params._id
        })

        res.status(200).send(httpMsg.M200)
    },

    getAllCategory: async (req, res) => {
        const categoryList = await CentralMerchandiseCategoryModel.find({}).skip(0).limit(0)
        res.send(categoryList)
    },

    getCategoryById: async (req, res) => {
        const categoryList = await CentralMerchandiseCategoryModel.find({
            _id: req.params._id
        })
            .skip(0)
            .limit(0)
        res.send(categoryList[0])
    }
}

module.exports = centralMerchandiseCategoryController
