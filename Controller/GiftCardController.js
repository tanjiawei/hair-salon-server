const giftCardModel = require('../Models/GiftCardModel');
const fs = require('fs');
const path = require('path');

const giftCardController = {
    getAllGiftCard: async (req, res) => {
        let cardList = await giftCardModel.find({
            isValid: true
        });
        res.json(cardList);
    },

    addGiftCard: async (req, res) => {
        let data = { ...req.body };
        if (req.file) {
            data.imgURL = path.basename(req.file.path);
        }
        let newEntry = new giftCardModel(data);
        await newEntry.save();
        res.status(200).send('Save Success');
    },

    updateGiftCard: async (req, res) => {
        let _id = req.params._id;
        let entry = await giftCardModel.findById(_id);
        let oldImgURL = entry.imgURL;
        await giftCardModel.updateOne(
            { _id: _id },
            {
                $set: {
                    ...req.body,
                    imgURL: req.file ? path.basename(req.file.path) : oldImgURL
                }
            }
        );
        if (req.file) {
            fs.unlinkSync(`public/image/${oldImgURL}`);
        }
        res.status(200).send('Update Success');
    },

    deleteGiftCard: async (req, res) => {
        let _id = req.params._id;
        await giftCardModel.updateOne(
            { _id: _id },
            {
                $set: {
                    isValid: false
                }
            }
        );
        res.status(200).send('Delete Success');
    },

    getImage: async (req, res) => {
        let imgPath = `public/image/${req.params.imgPath}`;
        if (fs.existsSync(imgPath)) {
            res.sendFile(imgPath, {
                root: `${__dirname}/../`
            });
        } else {
            res.status(404).send('Image do not exist');
        }
    },

    addGiftCardRecord: async (req, res) => {
        let data = { ...req.body };
        let newEntry = new giftCardModel(data);
        await newEntry.save();
        res.status(200).send('Save Success');
    }
};

module.exports = giftCardController;
