const fs = require('fs');
const _ = require('lodash');

const MerchandiseModel = require('../Models/MerchandiseModel');
const httpMsg = require('../Util/httpCustomMessage');

const MerchandiseController = {
    getAllMerchandises: async (req, res) => {
        // const merchandises = await MerchandiseModel.find({ adminid: mongoose.Types.ObjectId(req.query.adminid) })
        res.status(200).json(res.paginationResult);
    },

    getShortageList: async (req, res) => {
        let merchandiseList = await MerchandiseModel.aggregate([
            {
                $match: {
                    $and: [
                        {
                            isValid: true
                        },
                        {
                            $gt: ['$minStock', '$stock']
                        }
                    ]
                }
            },
            {
                $lookup: {
                    from: 'merchandisecategories',
                    localField: 'categoryId',
                    foreignField: '_id',
                    as: 'CategoryDoc'
                }
            }
        ]);
        res.json(merchandiseList);
    },

    addMerchandise: async (req, res) => {
        req.body.imgURL = req.body.imgURL ? JSON.parse(req.body.imgURL) : [];

        if (req.files[0])
            req.files.map((each) => req.body.imgURL.push(each.destination + each.filename));

        const merchandise = new MerchandiseModel({
            name: req.body.name,
            categoryId: req.body.categoryId,
            price: req.body.price,
            stock: req.body.stock,
            minStock: req.body.minStock ?? 0,
            note: req.body.note ?? '',
            isForService: req.body.isForService ?? false,
            adminid: req.body.adminid,
            imgURL: req.body.imgURL
        });

        await merchandise.save();
        res.send(merchandise);
    },

    updateMerchandise: async (req, res) => {
        const doc = await MerchandiseModel.findById(req.params._id);
        if (!doc) return res.status(404).send(httpMsg.M404);

        req.body.imgURL = JSON.parse(req.body.imgURL);

        if (_.difference(req.body.imgURL, doc.imgURL).length > 0)
            return res.status(400).send(httpMsg.M400);

        const deleted = _.differenceWith(doc.imgURL, req.body.imgURL, _.isEqual);

        if (req.files[0])
            req.files.map((each) => req.body.imgURL.push(each.destination + each.filename));

        const merchandise = await MerchandiseModel.findByIdAndUpdate(
            req.params._id,
            { $set: { ...req.body } },
            { new: true }
        );

        if (deleted.length > 0) deleted.map((url) => fs.unlink(url, (err) => console.error(err)));

        res.status(200).send(merchandise);
    },

    deleteMerchandise: async (req, res) => {
        const merchandise = await MerchandiseModel.findByIdAndUpdate(req.params._id, {
            $set: { isValid: false, imgURL: [] }
        });

        if (!merchandise) return res.status(404).send(httpMsg.M404);

        if (merchandise.imgURL[0])
            merchandise.imgURL.map((url) => fs.unlink(url, (err) => console.error(err)));

        res.send(httpMsg.M200);
    }
};

module.exports = MerchandiseController;
