const mongoose = require('mongoose');
const InventoryLogModel = require('../Models/InventoryLogModel');
const MerchandiseModel = require('../Models/MerchandiseModel');

const InventoryLogController = {
    getAllInventoryLogs: async (req, res) => {
        const { pageNumber, pageSize, id } = req.query;

        const logList = await InventoryLogModel.aggregate([
            {
                $match: {
                    adminId: mongoose.Types.ObjectId(req.query.id)
                }
            },
            {
                $lookup: {
                    from: 'merchandises',
                    localField: 'merchandiseId',
                    foreignField: '_id',
                    as: 'merchandiseDoc'
                }
            },
            {
                $lookup: {
                    from: 'employees',
                    localField: 'employeeId',
                    foreignField: '_id',
                    as: 'employeeDoc'
                }
            },
            {
                $lookup: {
                    from: 'admins',
                    localField: 'adminId',
                    foreignField: '_id',
                    as: 'adminDoc'
                }
            },
            {
                $project: {
                    'employeeDoc.passwordSalt': 0,
                    'employeeDoc.password': 0,
                    'adminDoc.passwordSalt': 0,
                    'adminDoc.password': 0
                }
            },
            {
                $sort: { createdAt: -1 }
            }
        ])
            .skip((Number(pageNumber) - 1) * Number(pageSize))
            .limit(Number(pageSize));

        if (!logList[0])
            return res.status(404).json({
                message: 'No logs found!'
            });

        const totalCount = await InventoryLogModel.countDocuments({ adminId: id });

        res.json({ totalCount, logList });
    },

    addInventoryLog: async (req, res) => {
        let newEntry = new InventoryLogModel({
            ...req.body
        });

        newEntry.save().then(() => {
            res.status(200).send('SaveSuccess');
        });

        let doc = await MerchandiseModel.findById(mongoose.Types.ObjectId(req.body.merchandiseId));
        await MerchandiseModel.updateOne(
            { _id: req.body.merchandiseId },
            {
                $set: {
                    stock: Number(doc.stock) + Number(req.body.stockLog)
                }
            }
        );
    }
};

module.exports = InventoryLogController;
