const mongoose = require('mongoose');
const categoryModel = require('../Models/CategoryModel');
const hrServiceModel = require('../Models/HRServiceModel');
const eventModel = require('../Models/EventModel');
const eventLogModel = require('../Models/EventLogModel');
const employeeModel = require('../Models/EmployeeModel');
const ReservationModel = require('../Models/ReservationModel');
const HRServiceBondModel = require('../Models/HRServiceBondModel');
const MessagerService = require('../Service/MessagerService');

const AppointmentController = {
    getAllEmployeeGroup: async (req, res) => {
        const employeeGroup = await categoryModel.find({}).sort({ name: 1 }).skip(0).limit(0);
        res.json(employeeGroup);
    },

    getAllEmployeeInGroupEvent: async (req, res) => {
        const employeeInGroup = await employeeModel.aggregate([
            {
                $match: {
                    adminid: mongoose.Types.ObjectId(req._id),
                    categoryId: mongoose.Types.ObjectId(req.body.categoryId)
                }
            },
            {
                $lookup: {
                    from: 'categories',
                    localField: 'categoryId',
                    foreignField: '_id',
                    as: 'docCategory'
                }
            },
            {
                $sort: {
                    'docEmployee.name': 1
                }
            }
        ]);
        res.json(employeeInGroup);
    },

    getAllEmployeeEvent: async (req, res) => {
        const employeeInGroup = await employeeModel.aggregate([
            {
                $match: {
                    adminid: mongoose.Types.ObjectId(req._id)
                }
            },
            {
                $lookup: {
                    from: 'categories',
                    localField: 'categoryId',
                    foreignField: '_id',
                    as: 'docCategory'
                }
            },
            {
                $sort: {
                    name: 1
                }
            }
        ]);
        res.json(employeeInGroup);
    },

    getAllServices: async (req, res) => {
        const doc = await hrServiceModel.find({}).sort({ _id: -1 }).skip(0).limit(0);
        res.json(doc);
    },

    getWeekEvents: async (req, res) => {
        const doc = await eventModel.aggregate([
            {
                $match: {
                    start: { $gte: new Date(req.body.weekstart), $lte: new Date(req.body.weekend) },
                    shopId: mongoose.Types.ObjectId(req._id),
                    status: { $ne: 'cancelled' }
                }
            },
            {
                $lookup: {
                    from: 'services',
                    localField: 'serviceId',
                    foreignField: '_id',
                    as: 'docServices'
                }
            },
            {
                $sort: {
                    'docServices._id': -1
                }
            }
        ]);
        res.json(doc);
    },

    getAllEmployee: async (req, res) => {
        const doc = await employeeModel.find({}).sort({ name: 1 }).skip(0).limit(0);
        res.json(doc);
    },

    addAppointmentEvent: async (req, res) => {
        const data = new eventModel({
            _id: new mongoose.Types.ObjectId(),
            employeeId: mongoose.Types.ObjectId(req.body.employeeId),
            serviceId: mongoose.Types.ObjectId(req.body.serviceId),
            day: req.body.day,
            start: req.body.start,
            end: req.body.end,
            duration: req.body.duration
        });

        data.save()
            .then(() => {
                res.status(200).send('Save Success');
            })
            .catch((err) => {
                console.log(err);
                res.status(500).send('Internal Server Error (' + err + ')');
            });
    },

    deleteAppointmentEvent: async (req, res) => {
        await eventModel
            .deleteOne({
                _id: mongoose.Types.ObjectId(req.body.eventId)
            })
            .then(() => {
                res.status(200).send('Delete Success');
            })
            .catch((err) => {
                console.error('Fail to update');
                res.status(500).send('Internal Server Error (' + err + ')');
            });
    },

    getAllReservation: async (req, res) => {
        const doc = await ReservationModel.aggregate([
            {
                $match: { shopId: mongoose.Types.ObjectId(req._id) }
            },
            {
                $lookup: {
                    from: 'events',
                    localField: 'events._id',
                    foreignField: '_id',
                    as: 'eventDetail'
                }
            },
            {
                $sort: { createTime: 1 }
            }
        ]);
        res.json(doc);
    },

    markEventStatus: async (req, res) => {
        const doc = await eventModel.updateOne(
            { _id: req.body._id },
            {
                $set: {
                    status: req.body.status,
                    statusMessage: req.body.statusMessage
                }
            }
        );
        const data = new eventLogModel({
            orderId: req.body.orderId,
            eventId: req.body.eventId,
            shopId: req.body.shopId,
            status: req.body.status,
            statusMessage: req.body.statusMessage
        });
        data.save().then(() => {
            if (req.body.status == 'cancelled') {
                MessagerService.sendMessageToEmployee(
                    {
                        shopId: req.body.shopId,
                        messageType: 'cancelled',
                        message: req.body.message,
                        userId: req.body.userId,
                        statusMessage: req.body.statusMessage
                    },
                    req.body.employeeId,
                    'Appointment Cancelled!',
                    'Your Appointment is cancelled!',
                    {
                        type: 'CancelAppointment'
                    }
                );
                MessagerService.sendMessageToUser(
                    {
                        shopId: req.body.shopId,
                        employeeId: req.body.employeeId,
                        messageType: 'cancelled',
                        message: req.body.message,
                        statusMessage: req.body.statusMessage
                    },
                    req.body.userId,
                    'Appointment Cancelled!',
                    'Your Appointment is cancelled!',
                    {
                        type: 'CancelAppointment'
                    }
                );
            }
            res.status(200).send('Update Success');
        });
    },

    getEmployeeByServiceAndTime: async (req, res) => {
        let tmpStart = new Date(req.body.searchDate);
        tmpStart.setHours(0, 0, 0, 0);
        let tmpEnd = new Date(req.body.searchDate);
        tmpEnd.setHours(24, 0, 0, 0);

        const servicebond = await HRServiceBondModel.aggregate([
            {
                $match: {
                    serviceId: mongoose.Types.ObjectId(req.body.serviceId)
                }
            },
            {
                $lookup: {
                    from: 'employees',
                    localField: 'employeeId',
                    foreignField: '_id',
                    as: 'employee'
                }
            },
            {
                $lookup: {
                    from: 'schedules',
                    let: { employee_id: '$employeeId' },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ['$employeeid', '$$employee_id'] },
                                        { $gte: ['$day', tmpStart] },
                                        { $lt: ['$day', tmpEnd] }
                                    ]
                                }
                            }
                        }
                    ],
                    as: 'schedules'
                }
            },
            {
                $unwind: '$schedules'
            },
            {
                $lookup: {
                    from: 'events',
                    let: { eId: '$schedules.employeeid', day: '$schedules.day' },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ['$employeeId', '$$eId'] },
                                        { $eq: ['$day', '$$day'] },
                                        { $ne: ['$status', 'cancelled'] }
                                    ]
                                }
                            }
                        }
                    ],
                    as: 'scheduleDoc'
                }
            }
        ]);
        if (servicebond === null) {
            res.status(500).json({ error: 'Internal Serve Error' });
        } else {
            res.status(200).json(servicebond);
        }
    }
};

module.exports = AppointmentController;
