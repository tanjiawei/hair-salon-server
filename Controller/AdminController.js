const fs = require('fs');
const mongoose = require('mongoose');
const JWT = require('jsonwebtoken');

const AdminModel = require('../Models/AdminModel');
const EmployeeModel = require('../Models/EmployeeModel');

const scheduleModel = require('../Models/ScheduleModel');
const employeeScheduleWeekModel = require('../Models/EmployeeScheduleModel');
const ShopOperationModel = require('../Models/ShopOperationModel');
const HRServiceBondModel = require('../Models/HRServiceBondModel');
const AppSettingModel = require('../Models/AppSettingModel');
const httpMsg = require('../Util/httpCustomMessage');

const Auth = require('../Service/AuthService');
const AdminService = require('../Service/AdminService');

const adminController = {
    login: async (req, res) => {
        const doc = await AdminModel.find({ userName: req.body.username });

        if (!doc[0]) return res.status(200).json({ error: 'Username missmatch' });

        const user = doc[0];
        if (Auth.checkPassword(req.body.password, user.password)) {
            const data = user.toObject();
            const jwt = JWT.sign(data._id.toString(), process.env.ADMIN_JWT_PRIVATE_KEY);

            res.status(200).json({ data, jwt });
        } else {
            res.status(200).json({ error: 'Password mismatch' });
        }
    },

    register: async (req, res) => {
        const findRes = await AdminService.findAdminByEmail(req.body.email);
        if (findRes) return res.status(400).json({ error: 'Email already in use' });

        const insertResult = await AdminService.addAdmin(
            req.body.email,
            req.body.address,
            req.body.password,
            false
        );

        if (insertResult) {
            const data = insertResult.toObject();
            const jwt = JWT.sign(data._id.toString(), process.env.ADMIN_JWT_PRIVATE_KEY);
            res.status(200).json({ data, jwt });
        }
    },

    changePassword: async (req, res) => {
        const decoded = JWT.decode(req.headers.authorization);

        if (req.body.newPassword !== req.body.newPasswordAgain)
            return res.status(400).json({ error: httpMsg.M400 });

        const admin = await AdminService.findAdminById(decoded);
        if (!admin) return res.status(400).json({ error: httpMsg.M400 });

        if (!Auth.checkPassword(req.body.password, admin.password))
            return res.status(401).json({ error: httpMsg.M401 });

        const result = await AdminService.replacePassword(decoded, req.body.newPassword);
        if (!result.ok) return res.status().json({ error: httpMsg.M500 });

        const jwt = JWT.sign(admin._id.toString(), process.env.ADMIN_JWT_PRIVATE_KEY);
        res.status(200).json({ data, jwt });
    },

    // creates a new employee account
    addEmployee: async (req, res) => {
        // check if email is in use
        // if not then add new user into data base

        const doc = await EmployeeModel.find({ email: req.body.email });

        if (doc[0]) return res.status(400).json({ error: 'Email already in use' });

        const data = new EmployeeModel({
            name: req.body.name,
            email: req.body.email,
            passwordSalt: salt,
            password: await Auth.encryptPassword(req.body.password),
            address: req.body.address,
            phone: req.body.phone,
            adminid: req._id,
            categoryId: req.body.categoryId
        });

        // saving new employee doc to db
        const employeeDocument = await data.save();
        // update admin employee list
        const adminDocument = await AdminModel.findOneAndUpdate(
            { _id: req._id },
            { $push: { employeeList: employeeDocument.toObject()._id } }
        );
        res.status(200).json(adminDocument);
    },

    getAllMyEmployee: async (req, res) => {
        const adminDoc = await AdminModel.findById(req._id);

        if (!adminDoc) return res.sendStatus(404);

        const employeeList = await EmployeeModel.find({
            adminid: mongoose.Types.ObjectId(req.query.id)
        });
        res.json(employeeList);
    },

    deleteEmployee: async (req, res) => {
        await EmployeeModel.deleteOne({ _id: mongoose.Types.ObjectId(req.body.id) });
        await HRServiceBondModel.deleteMany({
            employeeId: mongoose.Types.ObjectId(req.body.id)
        });
        await scheduleModel.deleteMany({ employeeid: mongoose.Types.ObjectId(req.body.id) });
        res.status(200).send('Delete Scucess!');
    },

    addMerchant: async (req, res) => {
        const duplicates = await AdminService.findAdminByUserName(req.body.userName);
        if (duplicates) return res.status(400).send('User Name already in use');

        const insertResult = await AdminService.addAdmin(
            req.body.userName,
            req.body.password,
            false,
            req.body.shopName,
            req.body.street1,
            req.body.street2,
            req.body.city,
            req.body.state,
            req.body.postalCode,
            req.body.country,
            req.body.shopTel
        );
        if (insertResult === null) return res.status(500).json({ error: 'Internal Server Error' });

        const data = insertResult.toObject();
        const jwt = JWT.sign(data._id.toString(), process.env.ADMIN_JWT_PRIVATE_KEY);
        res.status(200).json({ data, jwt });
    },

    editMerchant: async (req, res) => {
        await AdminModel.updateOne(
            { _id: mongoose.Types.ObjectId(req.body._id) },
            {
                $set: {
                    shopName: req.body.shopName,
                    street1: req.body.street1,
                    street2: req.body.street2,
                    city: req.body.city,
                    state: req.body.state,
                    postalCode: req.body.postalCode,
                    country: req.body.country,
                    street1: req.body.street1,
                    shopTel: req.body.shopTel,
                    userName: req.body.userName
                }
            }
        );
        res.status(200).send('Update Success');
    },

    getAllMerchant: async (req, res) => {
        const adminlist = await AdminService.findAdmin();
        res.status(200).json(adminlist);
    },

    getEmployeeSchedule: async (req, res) => {
        let docs = await scheduleModel.find({ employeeid: req.query.id });
        res.json({ docs });
    },

    postEmployeeSchedule: async (req, res) => {
        await scheduleModel.deleteMany({
            employeeid: req.body.employeeid,
            day: { $gte: req.body.start, $lte: req.body.end }
        });
        for (let i = 0; i < req.body.data.length; i++) {
            let doc = await scheduleModel.findOneAndUpdate(
                {
                    employeeid: req.body.employeeid,
                    day: req.body.data[i].day
                },
                {
                    scheduleType: req.body.data[i].scheduleType,
                    startTime: req.body.data[i].startTime,
                    endTime: req.body.data[i].endTime,
                    launchEnd: req.body.data[i].launchEnd,
                    launchStart: req.body.data[i].launchStart
                }
            );
            if (!doc) {
                const doc = new scheduleModel({
                    employeeid: req.body.employeeid,
                    day: req.body.data[i].day,
                    scheduleType: req.body.data[i].scheduleType,
                    startTime: req.body.data[i].startTime,
                    endTime: req.body.data[i].endTime,
                    launchEnd: req.body.data[i].launchEnd,
                    launchStart: req.body.data[i].launchStart,
                    adminid: req.body.employeeAdminid
                });
                await doc.save();
            }
        }
        res.status(200).json('Save success!');
    },
    getEmployeeScheduleWeek: async (req, res) => {
        let docs = await employeeScheduleWeekModel.aggregate([
            {
                $lookup: {
                    from: 'schedules',
                    localField: '_id',
                    foreignField: 'employeeid',
                    as: 'scheudles_docs'
                }
            }
        ]);
        res.json({ docs });
    },

    getEmployeeListByGroup: async (req, res) => {
        const docs = await scheduleModel.aggregate([
            {
                $match: {
                    adminid: mongoose.Types.ObjectId(req._id)
                }
            },
            {
                $group: {
                    _id: '$employeeid',
                    count: { $sum: 1 },
                    children: {
                        $push: {
                            day: '$day',
                            scheduleType: '$scheduleType',
                            startTime: '$startTime',
                            endTime: '$endTime'
                        }
                    }
                }
            },
            {
                $lookup: {
                    from: 'employees',
                    localField: '_id',
                    foreignField: '_id',
                    as: 'employee'
                }
            }
        ]);
        res.json({ docs });
    },
    getLocationInfo: async (req, res) => {
        const docLocationInfo = await AdminModel.find({
            _id: req._id
        })
            .skip(0)
            .limit(0);
        res.json(docLocationInfo);
    },
    getWeekOperation: async (req, res) => {
        const doc = await ShopOperationModel.find({
            shopDate: { $gte: req.body.dateFrom, $lte: req.body.dateTo },
            adminId: mongoose.Types.ObjectId(req._id)
        })
            .skip(0)
            .limit(0)
            .sort({ shopDate: 1 });
        res.json(doc);
    },
    getShopCloseDate: async (req, res) => {
        const doc = await ShopOperationModel.find({
            shopOpen: req.body.shopOpen,
            adminId: mongoose.Types.ObjectId(req._id)
        })
            .skip(0)
            .limit(0)
            .sort({ shopDate: 1 });
        res.json(doc);
    },
    addWeekOperation: async (req, res) => {
        const data = new AppSettingModel({
            adminId: req.body.adminId,
            shopDate: req.body.shopDate,
            shopOpenTime: req.body.shopOpenTime,
            shopCloseTime: req.body.shopCloseTime,
            shopOpen: req.body.shopOpen,
            shopCloseDesc: req.body.shopCloseDesc
        });
        await data.save();

        res.status(200).send('Save Success');
    },
    getReloadMoneySetting: async (req, res) => {
        const result = await AppSettingModel.find({ type: 'ReloadMoney' });
        res.status(200).json({ ok: true, data: result });
    },

    updateReloadMoneySetting: async (req, res) => {
        const result = await AppSettingModel.find({ type: 'ReloadMoney' });
        let content = [
            {
                pay: req.body.type1_pay,
                get: req.body.type1_get
            },
            {
                pay: req.body.type2_pay,
                get: req.body.type2_get
            },
            {
                pay: req.body.type3_pay,
                get: req.body.type3_get
            }
        ];
        if (result.length > 0) {
            AppSettingModel.updateOne(
                {
                    type: 'ReloadMoney'
                },
                {
                    $set: {
                        content: content
                    }
                }
            )
                .then(() => {
                    res.status(200).json({ ok: true });
                })
                .catch((err) => {
                    res.status(200).json({ ok: false, error: err });
                });
        } else {
            const data = new AppSettingModel({
                type: 'ReloadMoney',
                content: content
            });
            await data.save();

            res.status(200).json({ ok: true });
        }
    },
    getAdsSetting: async (req, res) => {
        const result = await AppSettingModel.find({ type: 'Ads' });
        res.status(200).json({ ok: true, data: result });
    },
    updateAdsSetting: async (req, res) => {
        const result = await AppSettingModel.find({ type: 'Ads' });

        let content = [];

        req.files.forEach((file) => {
            content.push(file.destination + file.filename);
        });

        if (result.length > 0) {
            if (result[0].content) {
                result[0].content.forEach((file) => {
                    fs.stat(file, (err, stats) => {
                        if (err && err.code === 'ENOENT') {
                        } else {
                            fs.unlinkSync(file);
                        }
                    });
                });
            }
            AppSettingModel.updateOne(
                {
                    type: 'Ads'
                },
                {
                    $set: {
                        content: content
                    }
                }
            )
                .then(() => {
                    res.status(200).json({ ok: true });
                })
                .catch((err) => {
                    res.status(200).json({ ok: false, error: err });
                });
        } else {
            const data = new AppSettingModel({
                type: 'Ads',
                content: content
            });
            await data.save();

            res.status(200).json({ ok: true });
        }
    },
    getPointsSetting: async (req, res) => {
        const result = await AppSettingModel.find({ type: 'Points' });
        res.status(200).json({ ok: true, data: result });
    },

    updatePointsSetting: async (req, res) => {
        const result = await AppSettingModel.find({ type: 'Points' });
        let content = [
            {
                key: 'share',
                value: req.body.share_value
            },
            {
                key: 'service',
                value: req.body.service_value
            },
            {
                key: 'complete',
                value: req.body.complete_value
            },
            {
                key: 'account',
                value: req.body.openAccount_value
            }
        ];
        if (result.length > 0) {
            AppSettingModel.updateOne(
                {
                    type: 'Points'
                },
                {
                    $set: {
                        content: content
                    }
                }
            )
                .then(() => {
                    res.status(200).json({ ok: true });
                })
                .catch((err) => {
                    res.status(200).json({ ok: false, error: err });
                });
        } else {
            const data = new AppSettingModel({
                type: 'Points',
                content: content
            });
            await data.save();

            res.status(200).json({ ok: true });
        }
    },
    getShopAddress: async (req, res) => {
        const adminlist = await AdminService.findAdmin();
        res.status(200).json(adminlist);
    }
};

module.exports = adminController;
