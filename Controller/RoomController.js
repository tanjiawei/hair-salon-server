const RoomModel = require('../Models/RoomModel.js');
const Mongoose = require('mongoose');

const RoomController = {
    // Fetch all the exist Room
    getAllRooms: async (req, res) => {
        const roomList = await RoomModel.find({ adminid: Mongoose.Types.ObjectId(req.query.id) });
        res.send(roomList);
    },

    // Add new room by name.
    addRoom: async (req, res) => {
        let newEntry = new RoomModel({ name: req.body.name, adminid: req.body.adminId });

        const result = await newEntry.save();
        res.status(200).send(result);
    },

    // Update an exist Room.
    updateRoom: async (req, res) => {
        const result = await RoomModel.updateOne(
            { _id: req.body._id },
            {
                $set: {
                    name: req.body.name
                }
            }
        );
        res.status(200).send(result);
    },

    // Delete Room by _id
    deleteRoom: async (req, res) => {
        let _id = req.params._id;
        await RoomModel.deleteOne({ _id: Mongoose.Types.ObjectId(_id) });

        res.status(200).send('Delete Success');
    }
};

module.exports = RoomController;
