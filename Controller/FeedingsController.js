const ms = require('ms')

const FeedingsModel = require('../Models/FeedingsModel')
const {
    cleanUp,
    validateParamsId,
    verifyBodyUserId
} = require('../Service/FeedingsService')
const httpMsg = require('../Util/httpCustomMessage')

const expiryInDays = 90

const FeedingsController = {
    getFeedById: async (req, res) => {
        const result = await FeedingsModel.find({
            _id: req.params._id,
            createdAt: {
                $gte: new Date(Date.now() - ms(`${expiryInDays}d`))
            }
        }).lean({ virtuals: true })

        res.send(result[0])
    },

    getAllFeeds: async (req, res) => {
        const result = await FeedingsModel.find({
            createdAt: {
                $gte: new Date(Date.now() - ms(`${expiryInDays}d`))
            }
        })
            .sort('-createdAt')
            .limit(100)
            .lean({ virtuals: true })

        res.send(result)

        // on query clean up old feedings together with old images in local folder
        const queries = await FeedingsModel.where('createdAt').lte(
            new Date(Date.now() - ms(`${expiryInDays}d`))
        )

        queries.map((q) => cleanUp(q))
    },

    getMyFeeds: async (req, res) => {
        const result = await FeedingsModel.find({
            'user._id': req.body.userId
        })
            .sort('-createdAt')
            .limit(100)
            .lean({ virtuals: true })

        res.send(result)
    },

    postFeed: async (req, res) => {
        const { imgURI, description = '' } = req.body
        // imgURI will be replaced with new array in middleware
        const user = await verifyBodyUserId(req, res)

        const feeding = new FeedingsModel({
            user: {
                _id: user._id, // this `_id` field supports the search by user function `getMyFeeds`
                name: user.name,
                points: user.points,
                profileImage: user.profileImage
            },
            imgURI,
            description
        })
        await feeding.save()

        res.send(feeding)
    },

    likeFeed: async (req, res) => {
        const feeding = await validateParamsId(req, res)

        const user = await verifyBodyUserId(req, res)

        const userInfo = {
            _id: user._id, // this `_id` field is for toggle between `like` and `unlike` and for future reference.
            name: user.name
        }

        const operation = {
            true: { $addToSet: { liked: userInfo } },
            false: {
                $pull: { liked: { _id: userInfo._id } }
            }
        }

        const result = await FeedingsModel.findByIdAndUpdate(
            req.params._id,
            operation[
                !feeding.liked.filter((obj) => obj._id.equals(userInfo._id))[0]
            ],
            { new: true }
        )

        res.send(result)
    },

    deleteFeed: async (req, res) => {
        const feeding = await validateParamsId(req, res)

        await cleanUp(feeding)
        res.send(httpMsg.M200)
    }
}

module.exports = FeedingsController
