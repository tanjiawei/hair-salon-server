const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const helmel = require('helmet');
const compression = require('compression');
const Fawn = require('fawn');

const CONFIG = require('./CONFIG');

const AdminRouter = require('./Routers/AdminRouter');
const EmployeeRouter = require('./Routers/EmployeeRouter');
const UserRouter = require('./Routers/UserRouter');
const HRServiceRouter = require('./Routers/HRServiceRouter');
const CategoryRouter = require('./Routers/CategoryRouter');
const HRServiceCategoryRouter = require('./Routers/HRServiceCategoryRouter');
const RoomRouter = require('./Routers/RoomRouter');
const EventRouter = require('./Routers/EventRouter');
const AppointmentRouter = require('./Routers/AppointmentRouter');
const MerchandiseRouter = require('./Routers/MerchandiseRouter');
const InventoryLogRouter = require('./Routers/InventoryLogRouter');
const MerchandiseCategoryRouter = require('./Routers/MerchandiseCategoryRouter');
const CentralMerchandiseRouter = require('./Routers/CentralMerchandiseRouter');
const OrderRouter = require('./Routers/OrderRouter');
const CentralInventoryLogRouter = require('./Routers/CentralInventoryLogRouter');
const CentralMerchandiseCategoryRouter = require('./Routers/CentralMerchandiseCategoryRouter');
const CentralAppCarousalRouter = require('./Routers/CentralAppCarousalRouter');
const FeedingsRouter = require('./Routers/FeedingsRouter');
const GiftCardRouter = require('./Routers/GiftCardRouter');
const GlobalError = require('./Middleware/Error');

require('express-async-errors');
const app = express();

app.use(cors());
app.use(helmel());
app.use(compression());

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json());
app.use('/user', UserRouter);
app.use('/employee', EmployeeRouter);
app.use('/admin', AdminRouter);
app.use('/service', HRServiceRouter);
app.use('/category', CategoryRouter);
app.use('/HRSCategory', HRServiceCategoryRouter);
app.use('/room', RoomRouter);
app.use('/order', OrderRouter);
app.use('/event', EventRouter);
app.use('/appointment', AppointmentRouter);
app.use('/merchandise', MerchandiseRouter);
app.use('/inventoryLog', InventoryLogRouter);
app.use('/merchandiseCategory', MerchandiseCategoryRouter);
app.use('/centralmerchandise', CentralMerchandiseRouter);
app.use('/CentralInventoryLog', CentralInventoryLogRouter);
app.use('/CentralAppCarousal', CentralAppCarousalRouter);
app.use('/CentralMerchandiseCategory', CentralMerchandiseCategoryRouter);
app.use('/feedings', FeedingsRouter);
app.use('/giftCard', GiftCardRouter);
app.use(GlobalError);

mongoose.connect(process.env.DB_LINK, {
    useFindAndModify: false,
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.connection.on('error', () => {
    console.log('DATA BASE CONNECTION ERROR');
});

Fawn.init(mongoose);

const port = process.env.PORT || 3030;
app.listen(port, () => {
    console.log('Listening on port: ' + port);
});

process.on('uncaughtException', (ex) => {
    console.log(ex.message);
});

process.on('unhandledRejection', (ex) => {
    console.log(ex.message);
});
