const express = require('express')
const InventoryLogController = require('../Controller/InventoryLogController')

const InventoryLogRouter = express.Router()

InventoryLogRouter.get(
    '/getAllInventoryLog',
    InventoryLogController.getAllInventoryLogs
)

InventoryLogRouter.post(
    '/addInventoryLog',
    InventoryLogController.addInventoryLog
)

module.exports = InventoryLogRouter