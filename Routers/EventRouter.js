const AuthenticateJWT = require('../Middleware/AuthenticateJWT')
const Express = require('express')
const EventController = require('../Controller/EventController')

const EventRouter = Express.Router()

EventRouter.get(
    '/getAllEvent',
    EventController.getAllEvents
)

EventRouter.post(
    '/addEvent',
    EventController.addEvent
)

EventRouter.put(
    '/updateEvent',
    EventController.updateEvent
)

EventRouter.delete(
    '/deleteEvent/:_id',
    EventController.deleteEvent
)

EventRouter.post(
    '/getEmployeeDateEvent',
    AuthenticateJWT.admin,
    EventController.getEmployeeDateEvent
)

module.exports = EventRouter
