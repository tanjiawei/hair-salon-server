const express = require('express');
const CentralMerchandiseController = require('../Controller/CentralMerchandiseController');
const { storage } = require('../Service/MerchandiseService');
const multer = require('multer');

const AuthenticateJWT = require('../Middleware/AuthenticateJWT');

const CentralMerchandiseRouter = express.Router();
const upload = multer({ storage });

// the sequence matters: escape authentication for the mobile app
CentralMerchandiseRouter.get('/:_id', CentralMerchandiseController.getMerchandisebyId);

CentralMerchandiseRouter.get('/', CentralMerchandiseController.getAllMerchandises);

CentralMerchandiseRouter.post('/bestsellers', CentralMerchandiseController.bestSellers);

CentralMerchandiseRouter.post(
    '/getMerchandiseByCategoryId',
    CentralMerchandiseController.getMerchandiseByCategoryId
);

CentralMerchandiseRouter.post('/getMerchandise', CentralMerchandiseController.getMerchandise);

CentralMerchandiseRouter.use(AuthenticateJWT.admin);

CentralMerchandiseRouter.put(
    '/:_id',
    upload.array('img'),
    CentralMerchandiseController.updateMerchandise
);

CentralMerchandiseRouter.delete('/:_id', CentralMerchandiseController.deleteMerchandise);

CentralMerchandiseRouter.post('/getShortageList/', CentralMerchandiseController.getShortageList);

CentralMerchandiseRouter.post(
    '/',
    upload.array('img'),
    CentralMerchandiseController.addMerchandise
);

module.exports = CentralMerchandiseRouter;
