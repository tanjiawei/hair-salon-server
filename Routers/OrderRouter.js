const Express = require('express')
const OrderController = require('../Controller/OrderController')
const AuthenticateJWT = require('../Middleware/AuthenticateJWT')

const OrderRouter = Express.Router()
OrderRouter.get(
    '/getAllOrders',
    OrderController.getAllOrders
).get('/getSingleOrder/:id', OrderController.getSingleOrder)
OrderRouter.post(
    '/createOrder',
    OrderController.createOrder
)
OrderRouter.post(
    '/createCentralMerchandiseOrder',
    AuthenticateJWT.user,
    OrderController.createCentralMerchandiseOrder
)
OrderRouter.post(
    '/getAllCentralMerchandiseOrder',
    OrderController.getAllCentralMerchandiseOrder
)
OrderRouter.post(
    '/getAllCentralMerchandiseOrderByIdOrDate',
    OrderController.getAllCentralMerchandiseOrderByIdOrDate
)
OrderRouter.post(
    '/getAllCentralMerchandiseShopOrder',
    AuthenticateJWT.admin,
    OrderController.getAllCentralMerchandiseShopOrder
)
OrderRouter.post(
    '/updateCentralMerchandiseStatus',
    OrderController.updateCentralMerchandiseStatus
)
OrderRouter.post(
    '/updateOrderUserId',
    OrderController.updateOrderUserId
)
.post('/updateOrderItem', OrderController.updateOrderItem)
.post('/addService', OrderController.addService)

OrderRouter.get(
    '/getOrdersByUserId/:id',
    OrderController.getOrdersByUserId
)
OrderRouter.get(
    '/getTransaction/:id',
    OrderController.getTransaction
)
OrderRouter.get(
    '/getOrdersByEmployeeId/:id',
    OrderController.getOrdersByEmployeeId
)

OrderRouter.get(
    '/getOrdersByShopId/:id',
    OrderController.getOrdersByShopId
)


OrderRouter.delete(
    '/deleteAllOrders',
    OrderController.deleteAllOrders
)
    .delete(
        '/deleteSingleOrder/:id',
        OrderController.deleteSingleOrder
    )
    .put(
        '/deleteOrderItems',
        OrderController.deleteOrderItems
    )

module.exports = OrderRouter
