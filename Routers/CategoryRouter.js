const express = require('express')
const categoryController = require('../Controller/CategoryController')
const categoryRouter = express.Router()

categoryRouter.get(
    "/getAllCategory",
    categoryController.getAllCategory
)

categoryRouter.post(
    "/addCategory",
    categoryController.addCategory
)

categoryRouter.put(
    "/updateCategory",
    categoryController.updateCategory
)

categoryRouter.post(
    "/deleteCategory",
    categoryController.deleteCategory
)

module.exports = categoryRouter
