const Express = require('express')
const RoomController = require('../Controller/RoomController')

const RoomRouter = Express.Router()

RoomRouter.get(
    '/getAllRoom',
    RoomController.getAllRooms
)

RoomRouter.post(
    '/addRoom',
    RoomController.addRoom
)

RoomRouter.put(
    '/updateRoom',
    RoomController.updateRoom
)

RoomRouter.delete(
    '/deleteRoom/:_id',
    RoomController.deleteRoom
)

module.exports = RoomRouter
