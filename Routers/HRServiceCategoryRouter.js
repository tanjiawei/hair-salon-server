const Express = require('express')
const HRServiceCategoryController = require('../Controller/HRServiceCategoryController')

const HRServiceCategoryRouter = Express.Router()

HRServiceCategoryRouter.get(
    '/getAllHRSCategory',
    HRServiceCategoryController.getAllCategories
)

HRServiceCategoryRouter.post(
    '/addHRSCategory',
    HRServiceCategoryController.addCategory
)

HRServiceCategoryRouter.put(
    '/updateHRSCategory',
    HRServiceCategoryController.updateCategory
)

HRServiceCategoryRouter.delete(
    '/deleteHRSCategory/:_id',
    HRServiceCategoryController.deleteCategory
)

module.exports = HRServiceCategoryRouter
