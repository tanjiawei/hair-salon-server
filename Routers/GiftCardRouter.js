const express = require('express')
const giftCardController = require('../Controller/GiftCardController')
const { storage } = require('../Service/MerchandiseService')
const multer = require('multer')
const upload = multer({ storage: storage })
const giftCardRouter = express.Router()

giftCardRouter.get(
    '/getAllGiftCard',
    giftCardController.getAllGiftCard
)

giftCardRouter.post(
    '/addGiftCard',
    upload.single('img'),
    giftCardController.addGiftCard
)

giftCardRouter.put(
    '/updateGiftCard/:_id',
    upload.single('img'),
    giftCardController.updateGiftCard
)

giftCardRouter.delete(
    '/deleteGiftCard/:_id',
    giftCardController.deleteGiftCard
)

giftCardRouter.get(
    '/getImg/:imgPath',
    giftCardController.getImage
)

giftCardRouter.post(
    '/addGiftCardRecord',
    giftCardController.addGiftCardRecord
)

module.exports = giftCardRouter