const express = require('express');
const multer = require('multer');

const CONFIG = require('../CONFIG');
const { storage } = require('../Service/FeedingsService');

const FeedingsController = require('../Controller/FeedingsController');
const imageResize = require('../Middleware/imageResize');

const FeedingsRouter = express.Router();
const upload = multer({ storage });

FeedingsRouter.post('/user', FeedingsController.getMyFeeds);

FeedingsRouter.get('/:_id', FeedingsController.getFeedById);

FeedingsRouter.get('/', FeedingsController.getAllFeeds);

FeedingsRouter.post(
    '/',
    [upload.array('img', CONFIG.feedingImageLimit), imageResize('feeding')],
    FeedingsController.postFeed
);

FeedingsRouter.put('/:_id', FeedingsController.likeFeed);

FeedingsRouter.delete('/:_id', FeedingsController.deleteFeed);

module.exports = FeedingsRouter;
