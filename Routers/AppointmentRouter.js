const express =  require('express')
const AppointmentRouter = express.Router()
const AuthenticateJWT = require('../Middleware/AuthenticateJWT')
const AuthenticateSuperAdmin = require('../Middleware/AuthenticateSuperAdmin')
const AppointmentController = require('../Controller/AppointmentController')

AppointmentRouter.post(
    "/getAllEmployeeGroup",
    AppointmentController.getAllEmployeeGroup
)

AppointmentRouter.post(
    "/getAllEmployeeInGroupEvent",
    AuthenticateJWT.admin,
    AppointmentController.getAllEmployeeInGroupEvent
)

AppointmentRouter.post(
    "/getAllEmployeeEvent",
    AuthenticateJWT.admin,
    AppointmentController.getAllEmployeeEvent
)

AppointmentRouter.post(
    "/getAllServices",
    AppointmentController.getAllServices
)

AppointmentRouter.post(
    "/getWeekEvents",
    AuthenticateJWT.admin,
    AppointmentController.getWeekEvents
)

AppointmentRouter.get(
    "/getAllEmployee",
    AppointmentController.getAllEmployee
)

AppointmentRouter.post(
    "/addAppointmentEvent",
    AppointmentController.addAppointmentEvent
)

AppointmentRouter.post(
    "/deleteAppointmentEvent",
    AppointmentController.deleteAppointmentEvent
)

AppointmentRouter.post(
    "/getAllReservation",
    AuthenticateJWT.admin,
    AppointmentController.getAllReservation
)

AppointmentRouter.post(
    "/markEventStatus",
    AppointmentController.markEventStatus
)

AppointmentRouter.post(
    "/getEmployeeByServiceAndTime",
    AppointmentController.getEmployeeByServiceAndTime
)

module.exports = AppointmentRouter