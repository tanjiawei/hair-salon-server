const express = require('express');
const CentralMerchandiseCategoryController = require('../Controller/CentralMerchandiseCategoryController');
const { storage } = require('../Service/MerchandiseService');
const multer = require('multer');

const AuthenticateJWT = require('../Middleware/AuthenticateJWT');
const merchandiseCategoryRouter = express.Router();
const upload = multer({ storage });

// the sequence matters: escape authentication for the mobile app
merchandiseCategoryRouter.get('/:_id', CentralMerchandiseCategoryController.getCategoryById);

merchandiseCategoryRouter.get('/', CentralMerchandiseCategoryController.getAllCategory);

merchandiseCategoryRouter.use(AuthenticateJWT.admin);

merchandiseCategoryRouter.post(
    '/',
    upload.single('img'),
    CentralMerchandiseCategoryController.addCategory
);

merchandiseCategoryRouter.put(
    '/:_id',
    upload.single('img'),
    CentralMerchandiseCategoryController.updateCategory
);

merchandiseCategoryRouter.delete('/:_id', CentralMerchandiseCategoryController.deleteCategory);

module.exports = merchandiseCategoryRouter;
