const express =  require('express')
const HRServiceController = require('../Controller/HRServiceController')
const HRServiceRouter = express.Router()
const AuthenticateJWT = require('../Middleware/AuthenticateJWT')
const AuthenticateSuperAdmin = require('../Middleware/AuthenticateSuperAdmin')

HRServiceRouter.post(
    "/addService",
    AuthenticateJWT.admin,
    HRServiceController.addService
)

HRServiceRouter.post(
    "/updateService",
    AuthenticateJWT.admin,
    HRServiceController.updateService
)

HRServiceRouter.post(
    "/deleteService",
    HRServiceController.deleteService
)

HRServiceRouter.get(
    "/getAllService",
    HRServiceController.getAllService
)

HRServiceRouter.get(
    "/getAllServicesCategory",
    HRServiceController.getAllServicesCategory
)

HRServiceRouter.post(
    "/addServiceBond",
    HRServiceController.addServiceBond
)

HRServiceRouter.post(
    "/deleteServiceBond",
    HRServiceController.deleteServiceBond
)

HRServiceRouter.post(
    "/getCheckedServiceBond",
    HRServiceController.getCheckedServiceBond
)

HRServiceRouter.get(
    "/getServiceMerchandiseBond/:serviceId",
    HRServiceController.getServiceMerchandiseBonds
)

HRServiceRouter.post(
    "/addServiceMerchandiseBond",
    HRServiceController.addServiceMerchandiseBonds
)

HRServiceRouter.delete(
    "/deleteServiceMerchandiseBond/:serviceId",
    HRServiceController.removeServiceMerchandiseBonds
)

module.exports = HRServiceRouter