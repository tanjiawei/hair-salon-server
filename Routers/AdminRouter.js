const express = require('express')
const AdminController = require('../Controller/AdminController')
const UserController = require('../Controller/UserController')
const AuthenticateJWT = require('../Middleware/AuthenticateJWT')
const AuthenticateSuperAdmin = require('../Middleware/AuthenticateSuperAdmin')
const CheckInput = require('../Middleware/CheckInput')
const multer = require('multer')
const path = require('path')

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/ads/')
    },

    filename: (req, file, cb) => {
        cb(null, file.fieldname + Date.now() + path.extname(file.originalname))
    }
})

const upload = multer({ storage: storage })
let adminRouter = express.Router()

adminRouter.post(
    '/login',
    CheckInput.username,
    CheckInput.password,
    AdminController.login
)

adminRouter.post(
    '/register',
    CheckInput.email,
    CheckInput.password,
    CheckInput.address,
    AdminController.register
)

adminRouter.post(
    '/changepassword',
    CheckInput.password,
    AuthenticateJWT.admin,
    AdminController.changePassword
)

adminRouter.get('/getAllMyEmployee', AuthenticateJWT.admin, AdminController.getAllMyEmployee)

adminRouter.post(
    '/addEmployee',
    AuthenticateJWT.admin,
    CheckInput.email,
    CheckInput.password,
    AdminController.addEmployee
)

adminRouter.post('/deleteEmployee', AuthenticateJWT.admin, AdminController.deleteEmployee)

adminRouter.post(
    '/addMerchant',
    AuthenticateJWT.admin,
    CheckInput.password,
    AdminController.addMerchant
)

adminRouter.put('/updateMerchant', AuthenticateJWT.admin, AdminController.editMerchant)

adminRouter.get(
    '/getAllMerchant',
    AuthenticateJWT.admin,
    AuthenticateSuperAdmin,
    AdminController.getAllMerchant
)

adminRouter.get('/getEmployeeSchedule', AuthenticateJWT.admin, AdminController.getEmployeeSchedule)

adminRouter.post(
    '/postEmployeeSchedule',
    AuthenticateJWT.admin,
    AdminController.postEmployeeSchedule
)

adminRouter.get('/getEmployeeScheduleWeek', AdminController.getEmployeeScheduleWeek)

adminRouter.get(
    '/getEmployeeListByGroup',
    AuthenticateJWT.admin,
    AdminController.getEmployeeListByGroup
)

adminRouter.get('/getLocationInfo', AuthenticateJWT.admin, AdminController.getLocationInfo)

adminRouter.post('/getWeekOperation', AuthenticateJWT.admin, AdminController.getWeekOperation)

adminRouter.post('/getShopCloseDate', AuthenticateJWT.admin, AdminController.getShopCloseDate)

adminRouter.post('/addWeekOperation', AdminController.addWeekOperation)

adminRouter.get('/getAllUsers', UserController.getAllUsers)

adminRouter.post('/updateReloadMoneySetting', AdminController.updateReloadMoneySetting)
adminRouter.get('/getReloadMoneySetting', AdminController.getReloadMoneySetting)

adminRouter.post('/updateAdsSetting', upload.array('img'), AdminController.updateAdsSetting)
adminRouter.get('/getAdsSetting', AdminController.getAdsSetting)

adminRouter.post('/updatePointsSetting', AdminController.updatePointsSetting)
adminRouter.get('/getPointsSetting', AdminController.getPointsSetting)
adminRouter.get('/getShopAddress', AdminController.getShopAddress)
module.exports = adminRouter
