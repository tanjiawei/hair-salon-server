const express = require('express');
const CentralAppCarousalController = require('../Controller/CentralAppCarousalController');
const CentralAppCarousalRouter = express.Router();
const AuthenticateJWT = require('../Middleware/AuthenticateJWT');

// the sequence matters: escape authentication for the mobile app
CentralAppCarousalRouter.get('/', CentralAppCarousalController.getListings);

CentralAppCarousalRouter.use(AuthenticateJWT.admin);

CentralAppCarousalRouter.post('/available', CentralAppCarousalController.getAvailableListings);

CentralAppCarousalRouter.post('/', CentralAppCarousalController.postListings);

CentralAppCarousalRouter.put('/:id', CentralAppCarousalController.editListings);

CentralAppCarousalRouter.delete('/:id', CentralAppCarousalController.deleteListings);

module.exports = CentralAppCarousalRouter;
