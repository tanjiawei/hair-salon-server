const express = require('express')
const multer = require('multer')

const CheckInput = require('../Middleware/CheckInput')
const imageResize = require('../Middleware/imageResize')

const AuthenticateJWT = require('../Middleware/AuthenticateJWT')
const UserController = require('../Controller/UserController')
const { storage } = require('../Service/UserService')

let userRouter = express.Router()
const upload = multer({ storage })

userRouter.post('/login', UserController.login)

userRouter.post(
    '/register',
    CheckInput.password,
    CheckInput.name,
    UserController.register
)

userRouter.get(
    '/loginWithJWT',
    AuthenticateJWT.user,
    UserController.loginWithJWT
)

userRouter.post('/makeReservation', UserController.makeReservation)

userRouter.get(
    '/getAllMerchants',
    AuthenticateJWT.user,
    UserController.getAllMerchants
)

userRouter.post(
    '/getEmployeeByIDList',
    AuthenticateJWT.user,
    CheckInput.idList,
    UserController.getEmployeeByIDList
)

userRouter.get(
    '/getAllServices',
    AuthenticateJWT.user,
    UserController.getAllServices
)

userRouter.get(
    '/getAllServicesByAdminID',
    UserController.getAllServicesByAdminID
)

userRouter.get('/getEmployeeByService', UserController.getEmployeeByService)

userRouter.get(
    '/getEmployeeByServiceAndTime',
    UserController.getEmployeeByServiceAndTime
)

userRouter.post(
    '/editProfile',
    [upload.array('img', 1), imageResize('user')],
    UserController.editProfile
)
userRouter.post(
    '/addPoints',
    //AuthenticateJWT.user,
    UserController.addPoints
)

userRouter.get(
    '/getEmployeeScheduleById/:id',
    UserController.getEmployeeScheduleById
)

userRouter.post('/restoreUserPassword', UserController.restoreUserPassword)

userRouter.post('/sendVerifyEmailCode', UserController.sendVerifyEmailCode)

userRouter.get(
    '/getEmployeeAvailableTime/',
    AuthenticateJWT.user,
    UserController.getEmployeeAvailableTime
)
userRouter.get(
    '/getMyReservation/',
    AuthenticateJWT.user,
    UserController.getMyReservation
)

userRouter.post(
    '/postCheckin/',
    AuthenticateJWT.user,
    UserController.postCheckin
)

userRouter.post('/getUserById', UserController.getUserById)

userRouter.post('/getUserByEmailOrPhone', UserController.getUserByEmailOrPhone)

userRouter.post('/postAddUserAddress', UserController.postAddUserAddress)

userRouter.post('/postEditUserAddress', UserController.postEditUserAddress)

userRouter.get(
    '/getMessages/',
    AuthenticateJWT.user,
    UserController.getMessages
)

userRouter.post('/bindFirebaseToken', UserController.bindFirebaseToken)

userRouter.post(
    '/createReload',
    AuthenticateJWT.user,
    UserController.createReload
)

userRouter.post(
    '/updateReload',
    AuthenticateJWT.user,
    UserController.updateReload
)

userRouter.post(
    '/purchaseOrder',
    AuthenticateJWT.user,
    UserController.purchaseOrder
)

userRouter.post(
    '/addFeedingPoint',
    AuthenticateJWT.user,
    UserController.addFeedingPoint
)

module.exports = userRouter
userRouter.get('/getCanadaPost', 
// AuthenticateJWT.user,
 UserController.getCanadaPost)

 userRouter.post('/postCreateNCSCanadaPost', 
AuthenticateJWT.user,
 UserController.postCreateNCSCanadaPost)

module.exports = userRouter
