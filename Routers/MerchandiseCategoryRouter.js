const express = require('express')
const merchandiseCategoryController = require('../Controller/MerchandiseCategoryController')
const AuthenticateJWT = require('../Middleware/AuthenticateJWT')
const multer = require('multer')
const { storage } = require('../Service/MerchandiseService')

const upload = multer({ storage })
const merchandiseCategoryRouter = express.Router()

merchandiseCategoryRouter.use(AuthenticateJWT.admin)

merchandiseCategoryRouter.get('/getAllCategory', merchandiseCategoryController.getAllCategory)

merchandiseCategoryRouter.post(
    '/addCategory',
    upload.single('img'),
    merchandiseCategoryController.addCategory
)

merchandiseCategoryRouter.put(
    '/updateCategory',
    upload.single('img'),
    merchandiseCategoryController.updateCategory
)

merchandiseCategoryRouter.delete(
    '/deleteCategory/:_id',
    merchandiseCategoryController.deleteCategory
)

module.exports = merchandiseCategoryRouter
