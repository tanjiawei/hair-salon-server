const express = require('express');
const CentralInventoryLogController = require('../Controller/CentralInventoryLogController');

const AuthenticateJWT = require('../Middleware/AuthenticateJWT');

const CentralInventoryLogRouter = express.Router();

CentralInventoryLogRouter.use(AuthenticateJWT.admin);

CentralInventoryLogRouter.get('/', CentralInventoryLogController.getAllInventoryLogs);

CentralInventoryLogRouter.post('/', CentralInventoryLogController.addInventoryLog);

module.exports = CentralInventoryLogRouter;
