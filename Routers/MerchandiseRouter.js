const Express = require('express')
const MerchandiseController = require('../Controller/MerchandiseController')
const multer = require('multer')
const { storage } = require('../Service/MerchandiseService')
const paginationResult = require('../Middleware/PaginationResult')
const MerchandiseModel = require('../Models/MerchandiseModel')

const MerchandiseRouter = Express.Router()
const upload = multer({ storage: storage })

MerchandiseRouter.get(
    '/getAllMerchandise',
    paginationResult(MerchandiseModel, {
        path: 'categoryId',
        select: '_id name'
    }),
    MerchandiseController.getAllMerchandises
)

MerchandiseRouter.get('/getShortageList', MerchandiseController.getShortageList)

// middleware here later

MerchandiseRouter.post('/addMerchandise', upload.array('img'), MerchandiseController.addMerchandise)

MerchandiseRouter.put(
    '/updateMerchandise/:_id',
    upload.array('img'),
    MerchandiseController.updateMerchandise
)

MerchandiseRouter.delete('/deleteMerchandise/:_id', MerchandiseController.deleteMerchandise)

module.exports = MerchandiseRouter
