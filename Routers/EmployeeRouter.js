const express = require("express");
const EmployeeController = require("../Controller/EmployeeController");
const CheckInput = require("../Middleware/CheckInput");
const AuthenticateJWT = require('../Middleware/AuthenticateJWT')

let employeeRouter = express.Router();

employeeRouter.post(
  "/login",
  CheckInput.email,
  CheckInput.password,
  EmployeeController.login
);

employeeRouter.post(
  "/register",
  CheckInput.email,
  CheckInput.password,
  EmployeeController.register
);

employeeRouter.post("/updateEmployee", EmployeeController.updateEmployee);

employeeRouter.post(
  "/updateEmployeePassword",
  CheckInput.email,
  CheckInput.password,
  EmployeeController.updateEmployeePassword
);

employeeRouter.post("/startEvent", EmployeeController.startEvent);

employeeRouter.post("/completeEvent", EmployeeController.completeEvent);
employeeRouter.post(
  "/getRangeEmployeeEvents",
  EmployeeController.getRangeEmployeeEvents
);

employeeRouter.post(
  "/bindFirebaseToken",
  EmployeeController.bindFirebaseToken
);

employeeRouter.get(
  '/getMessages',
  EmployeeController.getMessages
)

employeeRouter.get(
  '/loginWithJWT', 
  AuthenticateJWT.employee, 
  EmployeeController.loginWithJWT
)

module.exports = employeeRouter;
