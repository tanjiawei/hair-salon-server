const multer = require('multer')
const path = require('path')
const fs = require('fs')
const mongoose = require('mongoose')
const FeedingsModel = require('../Models/FeedingsModel')
const UserModel = require('../Models/UserModel')

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/feeding/')
    },

    filename: (req, file, cb) => {
        cb(null, file.fieldname + Date.now() + path.extname(file.originalname))
    }
})

const cleanUp = async (file) => {
    file.imgURI.map((uri) => fs.unlink(uri, (err) => (!err ? null : console.error(err))))

    await FeedingsModel.findByIdAndRemove(file._id)
}

const validateParamsId = async ({ params: { _id } }, res) => {
    if (!mongoose.Types.ObjectId.isValid(_id)) return res.status(400).send(httpMsg.M400)

    const feeding = await FeedingsModel.findById(_id)
    if (!feeding) return res.status(404).send(httpMsg.M404)

    return feeding
}
const verifyBodyUserId = async ({ body: { userId } }, res) => {
    if (!userId) return res.status(400).send(httpMsg.M400)

    const user = await UserModel.findById(userId)
    if (!user) return res.status(404).send(httpMsg.M404)

    return user
}

module.exports = { storage, cleanUp, validateParamsId, verifyBodyUserId }
