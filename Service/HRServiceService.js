const HRServiceModel = require("../Models/HRServiceModel")
const mongoose = require('mongoose')

const HRServiceService = {
    getAllServices: async () => {
        const serviceList = await HRServiceModel.aggregate([
            {
                $lookup: {
                    from: "hrservicecategories",
                    localField: "categoryId",
                    foreignField: "_id",
                    as: "CategoryDoc"
                }
            },
            {
                $lookup: {
                    from: "rooms",
                    localField: "roomId",
                    foreignField: "_id",
                    as: "RoomDoc"
                }
            }
        ])
        return serviceList
    },
    getAllServicesByID: async (id) => {
        const serviceList = await HRServiceModel.aggregate([
            {
                $match: {
                    adminid: mongoose.Types.ObjectId(id)
                }
            },
            {
                $lookup: {
                    from: "hrservicecategories",
                    localField: "categoryId",
                    foreignField: "_id",
                    as: "CategoryDoc"
                }
            },
            {
                $lookup: {
                    from: "rooms",
                    localField: "roomId",
                    foreignField: "_id",
                    as: "RoomDoc"
                }
            }
        ])
        return serviceList
    }
}

module.exports = HRServiceService