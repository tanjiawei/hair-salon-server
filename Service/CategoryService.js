const categoryModel = require("../Models/CategoryModel")

const categoryService = {
    getAllServices: async (skip = 0, limit = 0) => {
        try {
            return await categoryModel.find({}).skip(skip)
            .limit(limit)
        } catch(err) {
            console.log(err)
            return null
        }
    }
}

module.exports = categoryService
