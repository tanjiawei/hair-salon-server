const ScheduleModel = require("../Models/ScheduleModel")
var mongoose = require('mongoose')

const ScheduleService = {
    getSchedule: async (id, skip = 0, limit = 0) => {
        try {
            return await ScheduleModel.find({ 'employeeid': mongoose.Types.ObjectId(id) }).skip(skip).limit(limit)
        } catch (err) {
            console.log(err)
            return null
        }
    }
}

module.exports = ScheduleService