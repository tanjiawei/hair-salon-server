const multer = require('multer')
const path = require('path')

const InventoryLogModel = require('../Models/InventoryLogModel')
const MerchandiseModel = require('../Models/MerchandiseModel')
const CentralInventoryLogModel = require('../Models/CentralInventoryLogModel')
const CentralMerchandiseModel = require('../Models/CentralMerchandiseModel')

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/image/')
    },

    filename: (req, file, cb) => {
        cb(null, file.fieldname + Date.now() + path.extname(file.originalname))
    }
})

const MerchandiseService = {
    shopOrderInventoryLog: (
        merchandiseId,
        adminId,
        stockLog,
        employeeId,
        orderId,
        type = 'order'
    ) => {
        let newEntry = new InventoryLogModel({
            merchandiseId: merchandiseId,
            adminId: adminId,
            stockLog: stockLog,
            employeeId: employeeId,
            type: type,
            orderId: orderId
        })
        newEntry.save()
        MerchandiseModel.updateOne(
            { _id: merchandiseId },
            {
                $inc: {
                    stock: stockLog
                }
            }
        )
    },
    onlineOrderInventoryLog: (merchandise, stockLog, orderId) => {
        let newEntry = new CentralInventoryLogModel({
            merchandise: merchandise,
            stockLog: stockLog,
            type: 'order',
            orderId: orderId
        })
        newEntry.save()
        CentralMerchandiseModel.updateOne(
            { _id: merchandise },
            {
                $inc: {
                    stock: stockLog
                }
            }
        )
    }
}

module.exports = { MerchandiseService, storage }
