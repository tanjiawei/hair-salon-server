const EmployeeModel = require("../Models/EmployeeModel")

const EmployeeService = {
    findAllEmployeeByIDList: async (list = [], skip = 0, limit = 10) => {
        try {
            return await EmployeeModel.find({ "_id": { "$in": list } }).skip(skip).limit(limit)
        } catch (err) {
            console.log(err)
            return null
        }
    },

    getAllEmployees: async (skip = 0, limit = 0) => {
        try {
            return await EmployeeModel.find({}).skip(skip).limit(limit)
        } catch (err) {
            console.log(err)
            return null
        }
    },
    getEmployeesByService: async (serviceid, adminid, skip = 0, limit = 0) => {
        try {
            return await EmployeeModel.find({}).skip(skip).limit(limit)
        } catch (err) {
            console.log(err)
            return null
        }
    }
}

module.exports = EmployeeService