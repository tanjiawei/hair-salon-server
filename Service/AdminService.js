const AdminModel = require('../Models/AdminModel');
const Auth = require('../Service/AuthService');
const mongoose = require('mongoose');
const AdminService = {
    // merchant and admin is under the same collection called admin
    addAdmin: async (
        userName,
        password,
        superAdmin = false,
        shopName,
        street1,
        street2,
        city,
        state,
        postalCode,
        country,
        shopTel
    ) => {
        const data = new AdminModel({
            userName,
            password: await Auth.encryptPassword(password),
            superAdmin,
            shopName,
            street1,
            street2,
            city,
            state,
            postalCode,
            country,
            shopTel
        });

        try {
            return await data.save();
        } catch (err) {
            console.log(err);
            return null;
        }
    },

    findAdminByEmail: async (email = '') => {
        try {
            return await AdminModel.find({ email: email });
        } catch (err) {
            console.log(err);
            return [];
        }
    },

    findAdminByEmail: async (email = '') => {
        try {
            const document = await AdminModel.findOne({ email: email });
            return document;
        } catch (err) {
            console.log(err);
            return null;
        }
    },

    findAdminById: async (id) => {
        try {
            return await AdminModel.findById(id).select('password');
        } catch (err) {
            console.log(err);
            return null;
        }
    },

    replacePassword: async (id, password) => {
        try {
            await AdminModel.findByIdAndUpdate(id, {
                $set: {
                    password: await Auth.encryptPassword(password)
                }
            });
            return {
                ok: true
            };
        } catch (error) {
            console.log(err);
        }
    },

    findAdmin: async (skip = 0, limit = 0) => {
        try {
            const document = await AdminModel.find({ superAdmin: false }).skip(skip).limit(limit);
            return document;
        } catch (err) {
            console.log(err);
            return null;
        }
    },

    isSuperAdmin: async (_id = '') => {
        const findRes = await AdminModel.find({
            _id: mongoose.Types.ObjectId(_id),
            superAdmin: true
        });
        return findRes.length !== 0;
    },

    getAllMerchants: async (skip = 0, limit = 0) => {
        try {
            const merchants = await AdminModel.find({ superAdmin: false }).skip(skip).limit(limit);
            return merchants;
        } catch (err) {
            return null;
        }
    },
    findAdminByUserName: async (userName = '') => {
        try {
            const document = await AdminModel.findOne({ userName: userName });
            return document;
        } catch (err) {
            console.log(err);
            return null;
        }
    }
};

module.exports = AdminService;
