const MessagerModel = require('../Models/MessagerModel')
const EmployeeModel = require('../Models/EmployeeModel')
const UserModel = require('../Models/UserModel')
const admin = require('firebase-admin');

const MessagerService = {
    sendMessageToUser: async (messager = {}, userId = "", title = "", body = "", data = {}) => {
        const dataMessage = new MessagerModel({ userId: userId, type: 'user', ...messager })
        dataMessage.save()
        let user = await UserModel.findById(userId)
        if (user.tokens && user.tokens.length > 0) {
            const message = {
                notification: {
                    title: title,
                    body: body,
                },
                data: data,
                apns: {
                    payload: {
                        aps: {
                            badge: 0,
                        },
                    },
                },
                tokens: user.tokens,
            }
            admin.messaging().sendMulticast(message).then((response) => {
                console.log(response.successCount + ' messages were sent successfully');
            })
        }
    },
    sendMessageToEmployee: async (messager = {}, employeeId = "", title = "", body = "", data = {}) => {
        const dataMessage = new MessagerModel({ employeeId: employeeId, type: 'employee', ...messager })
        dataMessage.save()
        let employee = await EmployeeModel.findById(employeeId)
        if (employee.tokens && employee.tokens.length > 0) {
            const message = {
                notification: {
                    title: title,
                    body: body,
                },
                data: data,
                apns: {
                    payload: {
                        aps: {
                            badge: 0,
                        },
                    },
                },
                tokens: employee.tokens,
            }
            admin.messaging().sendMulticast(message).then((response) => {
                console.log(response.successCount + ' messages were sent successfully');
            })
        }
    },

}

module.exports = MessagerService