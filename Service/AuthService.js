const bcrypt = require('bcrypt');

module.exports = class Auth {
    static saltRounds = 10;

    static encryptPassword = async (password) => bcrypt.hash(password, this.saltRounds);

    static checkPassword = async (password, hash) => bcrypt.compare(password, hash);
};
