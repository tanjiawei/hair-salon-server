const UserModel = require('../Models/UserModel')
const scheduleModel = require('../Models/ScheduleModel')
const mongoose = require('mongoose')
const moment = require('moment')
const multer = require('multer')
const path = require('path')

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/user/')
    },

    filename: (req, file, cb) => {
        cb(null, file.fieldname + Date.now() + path.extname(file.originalname))
    }
})

const UserService = {
    editProfile: async (id, updateQuery) => {
        return await UserModel.findOneAndUpdate({ _id: id }, updateQuery, {
            new: true
        })
    },
    getEmployeeScheduleByDay: async (employeeid, day) => {
        const scheduleList = await scheduleModel.aggregate([
            {
                $match: {
                    employeeid: mongoose.Types.ObjectId(employeeid),
                    day: new Date(day)
                }
            },
            { $project: { day: 1, startTime: 1, endTime: 1, employeeid: 1 } },
            {
                $lookup: {
                    from: 'events',
                    let: { eId: '$employeeid', day: '$day' },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ['$employeeId', '$$eId'] },
                                        { $eq: ['$day', '$$day'] }
                                    ]
                                }
                            }
                        },
                        {
                            $project: {
                                start: 1,
                                duration: 1,
                                end: 1
                            }
                        }
                    ],
                    as: 'scheduleDoc'
                }
            }
        ])
        return scheduleList
    },
    whetherCross: (curTime, duration, events) => {
        let isCross = false
        let curStart = parseInt(new Date(curTime).getTime() / 1000)
        console.log(moment(curStart * 1000).format('MM-DD hh:mm a'))
        let curEnd = curStart + parseInt(duration) * 60
        console.log(moment(curEnd * 1000).format('MM-DD hh:mm a'))

        for (let i = 0; i < events.length; i++) {
            event_start = parseInt(new Date(events[i].start).getTime() / 1000)
            event_end = parseInt(new Date(events[i].end).getTime() / 1000)
            if (curEnd <= event_start + 1 || curStart + 1 >= event_end) {
            } else {
                console.log(events[i])
                isCross = true
                break
            }
        }
        return isCross
    },
    storage
}

module.exports = UserService
