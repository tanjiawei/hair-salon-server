const JWT = require('jsonwebtoken');
const AdminService = require('../Service/AdminService');

const AuthenticateJWT = {
    user: (req, res, next) => {
        auth(req, res, next, process.env.USER_JWT_PRIVATE_KEY);
    },

    employee: (req, res, next) => {
        auth(req, res, next, process.env.EMPLOYEE_JWT_PRIVATE_KEY);
    },

    admin: (req, res, next) => {
        auth(req, res, next, process.env.ADMIN_JWT_PRIVATE_KEY);
    }
};

function auth(req, res, next, privateKey) {
    const token = req.headers['authorization']?.replace('Bearer', '').trim();

    if (token === null) {
        res.sendStatus(401);
    } else {
        JWT.verify(token, privateKey, async (err, _id) => {
            if (err) {
                res.status(403).json({ error: 'Invalid Token' });
                return;
            }

            // req._id =
            //   _id === "superAdminId"
            //     ? req.headers["x-check-admin-id"]
            //     : _id;

            const isSuperAdmin = await AdminService.isSuperAdmin(_id);
            req._id = isSuperAdmin ? req.headers['x-check-admin-id'] : _id;
            next();
        });
    }
}

module.exports = AuthenticateJWT;
