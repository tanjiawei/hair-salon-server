const sharp = require('sharp')
const path = require('path')
const fs = require('fs')

const imageResize = (pathName) => async ({ files, body }, res, next) => {
    if (typeof pathName !== 'string')
        return console.error(
            new TypeError(
                'Input must to type `string`, for example: `folderName`',
                'Middleware/imageResize.js',
                6
            )
        )
    if (!files || files.length === 0) return next()
    const imgURI = []
    let index = 1

    const resizePromises = files.map(async (file) => {
        const output = `img${
            body.userId || body.id || body._id
        }-${Date.now()}-${index}.jpg`
        index++ // indexed to prevent same time over-written bugs
        await sharp(file.path)
            .resize(640)
            .jpeg({ quality: 100 })
            .toFile(path.resolve(`public/${pathName}/`, output))

        fs.unlink(file.destination + file.filename, (err) =>
            !err ? null : console.error(err)
        )

        imgURI.push(`public/${pathName}/` + output)
    })

    await Promise.all([...resizePromises])

    body.imgURI = imgURI

    next()
}

module.exports = imageResize
