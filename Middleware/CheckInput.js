const CheckInput = {
    username: (req, res, next) => {
        if (!req.body.username) {
            res.status(400).json({ error: 'User Name cannot be empty' });
        } else {
            next();
        }
    },

    email: (req, res, next) => {
        if (!req.body.email) {
            res.status(400).json({ error: 'Email cannot be empty' });
        } else {
            next();
        }
    },

    password: (req, res, next) => {
        if (!req.body.password) {
            res.status(400).json({ error: 'Password cannot be empty' });
        } else {
            next();
        }
    },

    idList: (req, res, next) => {
        if (!req.body.idList) {
            res.status(400).json({ error: 'ID list cannot be empty' });
            return;
        }

        if (!(req.body.idList instanceof Array)) {
            res.status(400).json({ error: 'ID list must be an array' });
            return;
        }

        if (req.body.idList.length === 0) {
            res.status.json({ error: 'ID list cannot be empty' });
            return;
        }

        next();
    },

    address: (req, res, next) => {
        if (!req.body.address) {
            res.status(400).json({ error: 'Address cannot be empty' });
            return;
        } else {
            next();
        }
    },

    name: (req, res, next) => {
        if (!req.body.name) {
            res.status(400).json({ error: 'name cannot be empty' });
            return;
        } else {
            next();
        }
    },

    shippingAddress: (req, res, next) => {
        if (!req.body.shippingAddress) {
            res.status(400).json({ error: 'Shipping address cannot be empty' });
            return;
        } else {
            next();
        }
    },

    validUserProfileKey: (req, res, next) => {
        const validKeys = new Set(['email', 'name', 'profileImage']);

        Object.keys(req.body).forEach((key) => {
            if (!validKeys.has(key)) {
                delete req.body[key];
            }
        });
        next();
    }
};

module.exports = CheckInput;
