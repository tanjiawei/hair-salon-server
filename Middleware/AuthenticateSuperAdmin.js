const AdminService = require('../Service/AdminService');

const AuthenticateSuperAdmin = async (req, res, next) => {
    const isSuperAdmin = await AdminService.isSuperAdmin(req._id);
    if (isSuperAdmin) {
        next();
    } else {
        return status(403);
    }
};

module.exports = AuthenticateSuperAdmin;
