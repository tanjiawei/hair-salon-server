const httpMsg = require('../Util/httpCustomMessage');
module.exports = function (err, _, res, next) {
    res.status(500).send(httpMsg.M500);
};
